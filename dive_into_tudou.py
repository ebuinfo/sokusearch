#!/usr/bin/python
# encoding=utf8


import requests
import re
import time


def dive_into_tudou(url):
    r = requests.get(url, timeout=20)
    if r.status_code == 404:
        return

    time.sleep(2)
    rreg = r',iid: .*[^\d]'
    iid = re.search(rreg, r.text).group()
    payload = {'jsoncallback':'page_play_model_itemSumModel__find', 'app':'4', 'showArea':'true', 'iabcdefg':iid.split()[1].encode('utf-8'), 'uabcdefg':'0', 'juabcdefg':'019dbmdt972c4n'}
    r1 = requests.get('http://www.tudou.com/crp/itemSum.action', params = payload)
    a = r1.text.split('{')[1].split('}')[0]
    b = a.split(',')
    nn = []
    for mm in b:
        nn.append(int(mm.split(':')[1]))

    # url_hash = fnvhash.fnv_64a_str(url)
    # site_url = 'tudou.com'
    # site_hash = fnvhash.fnv_32a_str('土豆')
    comment = nn[1]
    play = nn[8]
    down = None
    up = nn[4]

    return {'url':url, 'play':play, 'comment':comment, 'up':up, 'down':down}
