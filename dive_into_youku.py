#!/usr/bin/python
# encoding=utf8


import requests
import re
import time


def get_real_num(a):
    if type(a) == int:
        return a
    b = a.encode("utf-8").split(',')
    if len(b) == 4:
        d = int(b[0]) * 1000000000 + int(b[1]) * 1000000 + int(b[2]) * 1000 + int(b[3])
    if len(b) == 3:
        d = int(b[0]) * 1000000 + int(b[1]) * 1000 + int(b[2])
    if len(b) == 2:
        d = int(b[0]) * 1000 + int(b[1])
    if len(b) == 1:
        d = int(b[0])
    return d


def dive_into_youku(url):
# def dive_into_youku(self, url, kid, create_at, do_db):
    r = requests.get(url, timeout=5)
    time.sleep(2)
    if r.status_code == 404:
        return

    html = r.text
    # html = r.content
    vid_reg = r'var videoId \= \'(.*?)\''
    vid = re.search(vid_reg, r.content).groups()[0]

    # url_pre = 'http://v.youku.com/v_vpactionInfo/id/%s/pm/3?__rt=1&__ro=info_stat'
    # sec_url = url_pre % vid
    # print sec_url
    # html = requests.get(sec_url, timeout=5).text
    with open('youku.html', 'w') as f:
        f.write(r.content)
    # print html

    # get up/down
    reg_up = ur'顶:(.*?)\"'
    up_t = re.search(reg_up, html).groups()[0]
    up = get_real_num(up_t)
    reg_do = ur'踩:(.*?)\"'
    down_t = re.search(reg_do, html).groups()[0]
    down = get_real_num(down_t)

    # get comment
    co_url = 'http://comments.youku.com/comments/~ajax/getStatus.html?__ap={"videoid":"%s"}' % vid
    r = requests.get(co_url)
    reg = ur'评论.*?\"\>(.*?)\<'
    try:
        comment = re.search(r'"total":"(.*?)"', r.content).groups()[0]
    except:
        print r.content
        comment = re.search(r'"total":(.*?),', r.content).groups()[0]
    if comment == '':
        comment = 0
    # comment = get_real_num(comment)

    # get play
    pl_url = 'http://v.youku.com/QVideo/~ajax/getVideoPlayInfo?&id=%s&type=vv' % vid
    r = requests.get(pl_url)
    # print r.content
    reg = ur'总播放.*?\"\>(.*?)\<'
    play = re.search(r'"vv":(.*?),', r.content).groups()[0]

    rt = {'url':url, 'play':play, 'comment':comment, 'up':up, 'down':down}
    # print str(rt)
    rt = {'url':url, 'play':int(play), 'comment':int(comment), 'up':int(up), 'down':int(down)}

    # get danmu
    # get_danmu(vid)
    return rt


def get_danmu(vid):
    danmu_url = 'http://service.danmu.youku.com/list?3993&uid=23055631&mcount=1&mat=13&iid=%s&ct=1001&type=1' % vid
    dr = requests.post(danmu_url)
