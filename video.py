#!/usr/bin/env python
#coding=utf-8

from selenium import webdriver
import urllib
import re
from db_info import db
import fnvhash
import time
import random
import threading
import requests
#import unittest

SOUKU_URL = 'http://www.soku.com/'
OPEN = 1
CLOSE = 0
do_db = db['do']

class Get_info_from_souku_and_ori_source(object):

    def __init__(self, tv_name):
        self.mang_dir = {}
        self.toudou_tv_list = []
        self.youku_tv_list = []
        self.mangguo_tv_list = []
        self.leshi_tv_list = []
        self.qq_tv_list = []
        self.souhu_tv_list = []
        self.mangguo_tv_info_dir = {}
        self.youku_tv_info_dir = {}
        self.tudou_tv_info_dir = {}
        self.kankan_tv_list = []
        self.pps_tv_list = []
        self.iqiyi_tv_list = []
        self.wasu_tv_list = []
        self.mix_list = []
        self.i = 0
        self.tv_type = u''
        self.tv_num_dir = {}
        self.OUTLOG = 0
        self.undown_list = []
        self.log_list = []
        self.url_count = 0
        self.tv_name = tv_name
        self.do_db = db['self']
        sql = "SELECT id FROM crawl_words WHERE word='%s'" % self.tv_name
        self.craw_dir_list = self.do_db.query(sql)
        # print "craw_dir_list:", self.craw_dir_list
        self.kid = self.craw_dir_list[0]['id']
        # self.create_at = self.craw_dir_list[0]['create_at']
        self.mix_list = []
        self.clickSouku(tv_name)
        self.get_tv_list_from_second_page()


    def clickSouku(self, tv_name):
        '''点击搜库,得到节目url'''

        service_args = ['--load-images=false']
        driver = webdriver.PhantomJS(service_args=service_args)
        self.driver = driver
        # self.driver.set_window_size(1120, 550)
        self.driver.get(SOUKU_URL)
        self.driver.find_element_by_id('headq').send_keys(tv_name)
        self.driver.find_element_by_class_name('btn_search').click()
        self.second_page_url = self.driver.current_url
        self.driver.set_page_load_timeout(20)
        # print self.second_page_url
        # driver.close()

    def get_url_from_href_title_dianshiju(self, href):
        m = href.encode('utf-8').split("'")
        m = m[1].split('?')
        #print "m.split:",m
        self.mang_dir[m[0]] = 'true'
        return m[0]
        #print self.mang_dir.keys()
        #self.mix_list = self.mang_dir.keys()

    def get_list_from_href_title(self, href_title_list):
        for href in href_title_list:
            m = href.encode('utf-8').split('"')
            #print "m[1]:", m[1]
            m = m[1].split('?')
            #print "m.split:",m
            self.mang_dir[m[0]] = 'true'
            #print self.mang_dir.keys()
        self.mix_list = self.mang_dir.keys()

    def pick_up_tv_list_from_mix_list(self, mix_list):
        if self.OUTLOG == CLOSE:
            self.url_count = len(mix_list)
        self.toudou_tv_list = []
        self.youku_tv_list = []
        self.mangguo_tv_lisp = []
        self.souhu_tv_list = []
        self.iqiyi_tv_list = []
        self.kankan_tv_list = []
        self.qq_tv_list = []
        self.leshi_tv_list = []
        self.wasu_tv_list = []

        for http in mix_list:
            if 'www.tudou.com' in http:
                self.toudou_tv_list.append(http)
            if 'v.youku.com' in http:
                self.youku_tv_list.append(http)
            if 'www.hunantv.com' in http:
                self.mangguo_tv_list.append(http)
            if 'tv.sohu.com' in http:
                self.souhu_tv_list.append(http)
            if 'www.letv.com' in http:
                self.leshi_tv_list.append(http)
            if 'v.qq.com' in http:
                self.qq_tv_list.append(http)
            if 'vod.kankan.com' in http:
                self.kankan_tv_list.append(http)
            if 'pps.tv' in http:
                self.pps_tv_list.append(http)
            if 'iqiyi.com' in http:
                self.iqiyi_tv_list.append(http)
            if 'wasu.cn' in http:
                self.wasu_tv_list.append(http)
        if len(self.iqiyi_tv_list):
            print "iqiyi"
            print len(self.iqiyi_tv_list)
        if len(self.toudou_tv_list):
            print "tudou"
            print len(self.toudou_tv_list)
        if len(self.youku_tv_list):
            print "youku"
            print len(self.youku_tv_list)
        if len(self.mangguo_tv_list):
            print "mangguo"
            print len(self.mangguo_tv_list)
        if len(self.souhu_tv_list):
            print "sohu"
            print len(self.souhu_tv_list)
        if len(self.leshi_tv_list):
            print "letv"
            print len(self.leshi_tv_list)
        if len(self.qq_tv_list):
            print "qq"
            print len(self.qq_tv_list)
        if len(self.kankan_tv_list):
            print "kankan"
            print len(self.kankan_tv_list)
        if len(self.pps_tv_list):
            print "pps"
            print len(self.pps_tv_list)
        if len(self.wasu_tv_list):
            print "wasu:"
            print len(self.wasu_tv_list)

    def get_list_under_level(self, nw_url):
        #get url list from more
        page = urllib.urlopen(nw_url)
        print "nw_url:", nw_url
        data = page.read().decode('utf-8')
        # \u64ad\u653e\u6e90 : 播放源
        resu = re.search(ur"[\w\W]*?\u64ad\u653e\u6e90", data)
        #resu = re.search(ur"\u64ad\u653e\u6e90", data)
        #print "resu:",resu
        next_page = resu.group()
        try:
            #\u67e5\u770b\u5168\u90e8 : 查看全部
            su = re.findall(ur"accordion\-more[\w\W]*?\u67e5\u770b\u5168\u90e8", next_page)
        except:
            print "except!!!!"
        if su:
            #需要进一步展开获取全部url
            for mm in su:
                harf_url = mm.split('"')[2]
                new_url = u'http://www.soku.com' + harf_url
                page = urllib.urlopen(new_url)
                data = page.read().decode('utf-8')
                #\u6536\u8d77 : 收起
                resu = re.search(ur"[\w\W]*\u6536\u8d77", data)
                next_page = resu.group()
                href_title_list = re.findall(r"href=\"http.*?title=", next_page)
                #print "href_title_list:", href_title_list
                self.get_list_from_href_title(href_title_list)
        else:
            #不需要全部展开,只需要获取当前url
            page = urllib.urlopen(nw_url)
            data = page.read().decode('utf-8')
            # \u64ad\u653e\u6e90 : 播放源
            resu = re.search(ur"[\w\W]*?\u64ad\u653e\u6e90", data)
            #print resu.group()
            next_page = resu.group()
            href_title_list = re.findall(r"href=\"http.*?title=", next_page)
            self.get_list_from_href_title(href_title_list)

    def get_zongyi_list(self):
        #print "nav:"
        data = urllib.urlopen(self.second_page_url).read().decode('utf-8')
        #resu = re.search(ur"\u64ad\u653e\u6e90", data).group()
        resu = re.search(ur"[\w\W]*?\u64ad\u653e\u6e90", data).group()
        level = re.findall(r'\/v\?keyword\=.*?\" \_log\_title', resu)
        # print "level:", level
        if u'第一季' in self.tv_name:
            print "under_season_1@@"
            nw_url = self.second_page_url
            print "season_1_nw_url:",nw_url
            self.get_list_under_level(nw_url.encode('utf-8'))
        elif u'第二季' in self.tv_name:
            print "under_season_2@@"
            nw_url = self.second_page_url
            self.get_list_under_level(nw_url.encode('utf-8'))
        elif u'第三季' in self.tv_name:
            print "under_season_3@@"
            nw_url = self.second_page_url
            self.get_list_under_level(nw_url.encode('utf-8'))
        else:
            print "[*] under_season_all@@"
            self.get_list_under_level(self.second_page_url.encode('utf-8'))
            if level:
                for e_level in level:
                    nw_url = 'http://www.soku.com' + e_level.split('"')[0]
                    #print nw_url
                    self.get_list_under_level(nw_url.encode('utf-8'))
                    #break

    def get_new_dianshiju_list(self, second_page_url):
        page = urllib.urlopen(second_page_url)
        data = page.read().decode('utf-8')
        # \u64ad\u653e\u6e90 : 播放源
        # \u7684\u76f8\u5173\u8282\u76ee :的相关节目
        # \u7efc\u5408\u6392\u5e8f : 综合排序
        resu = re.search(ur"[\w\W]*?\u7684\u76f8\u5173\u8282\u76ee", data)
        if resu == None:
            resu = re.search(ur"[\w\W]*?\u7efc\u5408\u6392\u5e8f", data)

        #print resu.group()
        next_page = resu.group()
        href_title_list = re.findall(r"href=\'http.*?\<", next_page)
        for href in href_title_list:
            m_url = self.get_url_from_href_title_dianshiju(href)
            m_num = href.split('>')[1].split('<')[0]
            self.tv_num_dir[m_url] = m_num
            #print "tv_num", self.tv_num_dir
        self.mix_list = self.mang_dir.keys()

    def get_tv_list_from_second_page(self):
        '''give the second_page_url, get the tv_list'''

        self.driver.get(self.second_page_url)
        try:
            tv_type = self.driver.find_element_by_class_name("base_type").text
            self.tv_type = tv_type
            print "tv_type:", tv_type
            if tv_type == u'综艺':
                self.get_zongyi_list()
            # if u'电视剧' in tv_type or u'纪录片' in tv_type or u'动漫' in tv_type:
            else:
                print "second_page_url:", self.second_page_url
                self.get_new_dianshiju_list(self.second_page_url)
        except Exception, e:
            print e
            print "incorrect tv name!"

    def make_info_dir_video_and_data(self, url, url_hash, title, date, site_hash, site_url):
        info_dir_vedio = {}

        info_dir_vedio['url'] = url
        info_dir_vedio['url_hash'] = url_hash
        info_dir_vedio['video_name'] = title
        info_dir_vedio['video_date'] = date
        info_dir_vedio['site_hash'] = site_hash
        info_dir_vedio['site_url'] = site_url
        info_dir_vedio['kid'] = self.kid

        info_dir = {
            'info_dir_vedio': info_dir_vedio}
        return info_dir


    def dive_into_xunlei(self, url, self_driver):
        self_driver.get(url)
        time.sleep(1)

        if u'电视剧' in self.tv_type:
            title = self.tv_name + " " + self.tv_num_dir[url]
        else:
            title = self.tv_name
        # print "title:", title

        tvinfo_temp = self_driver.find_element_by_class_name("movieinfo_tt").text
        date = int(re.search(r'[0-9]+', tvinfo_temp).group())
        # print "date:", date

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'vod.kankan.com'
        site_hash = fnvhash.fnv_32a_str('迅雷')
        info_dir = self.make_info_dir_video_and_data(url, url_hash, title, date, site_hash, site_url)
        do_db = db
        do_db.item_to_table('video', info_dir['info_dir_vedio'])
        #self.do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])

    def dive_into_iqiyi(self, url):
        r = requests.get(url, timeout=20).content
        # get date
        reg = r'datePublished\" content\=\"([\w\W]*?)\"'
        try:
            date = re.search(reg, r).groups()[0]
            date = date.replace('年', '').replace('月', '').replace('日', '')
        except:
            reg = r'tvYear\:(.*?)\n'
            date = re.search(reg, r).groups()[0]
            date = date.split('\n')[0].replace(',', '')
        # get title
        reg = r'title\>([\w\W]*?)\<\/title'
        title = re.search(reg, r).groups()[0]
        title = title.replace('&nbsp;', '').replace('-视频在线观看-脱口秀-爱奇艺', '')
        print title

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'iqiyi.com'
        site_hash = fnvhash.fnv_32a_str('奇艺')
        info_dir = self.make_info_dir_video_and_data(url, url_hash, title, date, site_hash, site_url)
        do_db = db['iqiyi']
        do_db.item_to_table('video', info_dir['info_dir_vedio'])

    def dive_into_leshi(self, url):
        import requests
        html = requests.get(url, timeout=20).content
        reg = r'title\:\"([\w\W]*?)\"\,'
        # reg = r'title\>([\w\W]*?)\<\/title'
        title = re.search(reg, html).groups()[0]
        reg = r'releasedate\:\'(.*?)\'\,'
        date = re.search(reg, html).groups()[0]
        r = date.split(' ')[0].replace('-', '')
        if r != '': date = int(r)
        else:
            reg = r'\d{8}|\d{6}'
            date = int(re.search(reg, title).group())
            if date >100000 and date<1000000: date += 20000000
        if date < 10000:
            date = 0
        print title, date

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'letv.com'
        site_hash = fnvhash.fnv_32a_str('乐视')
        info_dir = self.make_info_dir_video_and_data(url, url_hash, title, date, site_hash, site_url)
        do_db = db['leshi']
        do_db.item_to_table('video', info_dir['info_dir_vedio'])

    def dive_into_qq(self, url, self_driver):
        r = requests.get(url, timeout=5)
        content = r.content
        # get title
        reg = r'title\>([\w\W]*?)\<\/title'
        title = re.search(reg, content).groups()[0]
        title = title.replace('- 高清在线观看 - 腾讯视频', '')
        reg = r'\d{4}-\d{2}-\d{2}'
        try:
            date = re.search(reg, title).group()
            date = date.replace('-', '')
        except:
            print title
        print title, date

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'v.qq.com'
        site_hash = fnvhash.fnv_32a_str('腾讯')
        info_dir = self.make_info_dir_video_and_data(url, url_hash, title, date, site_hash, site_url)
        do_db = db['qq']
        do_db.item_to_table('video', info_dir['info_dir_vedio'])
        # self.do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])

    def dive_into_tudou(self, url):
        import requests

        r = requests.get(url)
        if r.status_code == 404:
            return

        rreg = r',iid: .*[^\d]'
        iid = re.search(rreg, r.text).group()
        rreg1 = r',kw: \'.*?\''
        kw = re.search(rreg1, r.text).group()
        title = kw.split("'")[1]
        payload = {'jsoncallback':'page_play_model_itemSumModel__find', 'app':'4', 'showArea':'true', 'iabcdefg':iid.split()[1].encode('utf-8'), 'uabcdefg':'0', 'juabcdefg':'019dbmdt972c4n'}
        r1 = requests.get('http://www.tudou.com/crp/itemSum.action', params = payload)
        a = r1.text.split('{')[1].split('}')[0]
        b = a.split(',')
        nn = []
        for mm in b:
            nn.append(int(mm.split(':')[1]))

        rreg2 = r",kw: \'[\w\W]*?,olw:"
        pub = re.search(rreg2, r.text.encode('utf-8')).group()
        date_temp = pub.split('\r\n')[-2].split("'")[1].split('-')

        try:
            date = int(date_temp[0]) * 10000 + int(date_temp[1]) * 100 + int(date_temp[2])
        except:
            date = 0

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'tudou.com'
        site_hash = fnvhash.fnv_32a_str('土豆')

        info_dir = self.make_info_dir_video_and_data(url, url_hash, title, date, site_hash, site_url)
        do_db = db['tudou']
        do_db.item_to_table('video', info_dir['info_dir_vedio'])
        # self.do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])

    def dive_into_sohu(self, url):
        if 'v.tv.sohu.com' in url:
            return

        r = requests.get(url)
        content = r.content.decode('gbk')
        reg = r'title\>([\w\W]*?)\<\/title'
        title = re.search(reg, content).groups()[0]
        if u'404页' in title:
            return
        title = title.replace(u'- 搜狐视频', '')
        reg = r'\d{8}'
        try:
            date = int(re.search(reg, title).group())
        except:
            date = 0
        print title, date

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'tv.sohu.com'
        site_hash = fnvhash.fnv_32a_str('搜狐')
        info_dir = self.make_info_dir_video_and_data(url, url_hash, title, date, site_hash, site_url)
        do_db = db['sohu']
        do_db.item_to_table('video', info_dir['info_dir_vedio'])

        # print "comment:", comment
    def dive_into_youku(self, url):
        r = requests.get(url, timeout=10)
        id_html = r.text
        reg = r'title\>([\w\W]*?)\<\/title'
        title = re.search(reg, id_html).groups()[0]
        title = title.replace(u'—综艺—优酷网，视频高清在线观看', '')
        title =  title.replace(u'—在线播放', '')
        title =  title.replace(u'—电视剧—优酷网，视频高清在线观看', '')
        title =  title.replace(u'《', '').replace(u'》', '')

        reg = r'stage\=\"(.*?)\"'
        r = re.search(reg, id_html).groups()[0]
        if r != '': date = int(re.search(reg, id_html).groups()[0])
        else:
            reg = r'\d{8}|\d{6}'
            date = int(re.search(reg, title).group())
            if date >100000 and date<1000000: date += 20000000
        if date < 10000:
            date = 0
        print title, date

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'v.youku.com'
        site_hash = fnvhash.fnv_32a_str('优酷')
        # good = up
        # bad = down
        info_dir = self.make_info_dir_video_and_data(url, url_hash, title, date, site_hash, site_url)
        do_db = db['youku']
        do_db.item_to_table('video', info_dir['info_dir_vedio'])

    def dive_into_mangguo(self, url, self_driver):
        page = urllib.urlopen(url)
        page_data = page.read()
        reg = r'release_date\: ".*?\"'
        imgre = re.compile(reg)
        imglist = re.search(imgre, page_data).group()
        m = imglist.split('"')
        try:
            self_driver.get(url)
            time.sleep(2)
        except:
            pass
        title = self_driver.find_element_by_class_name('play-index-til').text
        h = m[1].split('-')
        date = int(h[0]) * 10000 + int(h[1]) * 100 + int(h[2])

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'hunantv.com'
        site_hash = fnvhash.fnv_32a_str('芒果')
        # good = up
        # bad = down
        info_dir = self.make_info_dir_video_and_data(url, url_hash, title, date, site_hash, site_url)
        do_db = db['mangguo']
        do_db.item_to_table('video', info_dir['info_dir_vedio'])

    def dive_into_wasu(self, url):
        r = requests.get(url, timeout=10)
        id_html = r.text
        reg = r'title\>([\w\W]*?)\<\/title'
        title = re.search(reg, id_html).groups()[0]
        title = title.replace(u'-最新、最全的电视节目-华数TV', '')

        reg = r'\d{8}'
        try:
            date = re.search(reg, title).group()
            date = int(date)
        except:
            date = 0
        print title, date

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'wasu.cn'
        site_hash = fnvhash.fnv_32a_str('华数')
        info_dir = self.make_info_dir_video_and_data(url, url_hash, title, date, site_hash, site_url)
        do_db = db['wasu']
        do_db.item_to_table('video', info_dir['info_dir_vedio'])


    def dive_into(self, site, site_tv_list, s):
        service_args = ['--load-images=false']
        self_driver = webdriver.PhantomJS(service_args=service_args)
        self_driver.set_page_load_timeout(s)

        sum = len(site_tv_list)
        i = 0
        for url in site_tv_list:
            i += 1
            print "[*] %s remain %d" % (site, sum-i)
            try:
                if site == 'youku':
                    self.dive_into_youku(url, self_driver)
            except:
                print "[*] %s PASS %s" % (site, url)
                open("outlog_video_pass.log", 'a').write("%s\tpass\t%s\n" % i(site, url))
        print "[*] %s down" % site
        self_driver.close()

    def get_tv_info_from_youku(self):
        sum = len(self.youku_tv_list)
        i = -1
        for url in self.youku_tv_list:
            i += 1
            print "youku", sum-i
            try:
                self.dive_into_youku(url)
                time.sleep(2)
            except:
                print "[*] YOUKU PASS", url
                open("outlog_video_pass.log", 'a').write("youku\tpass\t%s\n" % url)
                if self.OUTLOG == OPEN:
                    self.undown_list.append(url)
                if self.OUTLOG == CLOSE:
                    self.log_list.append(url)
        print "[*] youku down"
        open("all_video_log.log", 'a').write('[*]youku down\n')
        open("outlog_video_down.log", 'a').write("[*]YOUKU down\n")

    def get_tv_info_from_sohu(self):
        sum = len(self.souhu_tv_list)
        i = -1
        for url in self.souhu_tv_list:
            i += 1
            # print "[*] star-time-sohu:", time.ctime()
            print "sohu", sum-i
            try:
                self.dive_into_sohu(url)
                time.sleep(2)
            except Exception, e:
                print "[*]SOHU PASS ", url
                print "e:\n", e
                open("outlog_video_pass.log", 'a').write("sohu\tpass\t%s\n" % url)
                if self.OUTLOG == OPEN:
                    self.undown_list.append(url)
                if self.OUTLOG == CLOSE:
                    self.log_list.append(url)
            # self.dive_into_sohu(url, self_driver)
        print "[*] sohu down"
        open("all_video_log.log", 'a').write('[*]sohu down\n')
        open("outlog_video_down.log", 'a').write("[*]sohu down\n")

    def get_tv_info_from_mangguo(self):
        service_args = ['--load-images=false']
        self_driver = webdriver.PhantomJS(service_args=service_args)
        self_driver.set_page_load_timeout(8)

        sum = len(self.mangguo_tv_list)
        i = -1
        for url in self.mangguo_tv_list:
            i += 1
            print "mangguo", sum-i
            try:
                self.dive_into_mangguo(url, self_driver)
            except:
                print "[*] MANGGUO PASS ", url
                open("outlog_video_pass.log", 'a').write("mangguo\tpass\t%s\n" % url)
                if self.OUTLOG == OPEN:
                    self.undown_list.append(url)
                if self.OUTLOG == CLOSE:
                    self.log_list.append(url)
        self_driver.close()
        print "[*] mangguo down"
        open("all_video_log.log", 'a').write('[*]mangguo down\n')

    def get_tv_info_from_leshi(self):
        sum = len(self.leshi_tv_list)
        i = -1
        for url in self.leshi_tv_list:
            i += 1
            print "le", sum-i
            # try:
            self.dive_into_leshi(url)
            time.sleep(2)
            # except:
            #     print "[*] leshi PASS ", url
            #     open("outlog_video_pass.log", 'a').write("sohu\tpass\t%s\n" % url)
            #     if self.OUTLOG == OPEN:
            #         self.undown_list.append(url)
            #     if self.OUTLOG == CLOSE:
            #         self.log_list.append(url)
        print "[*] leshi down"
        open("all_video_log.log", 'a').write('[*]letv down\n')

    def get_tv_info_from_iqiyi(self):
        sum = len(self.iqiyi_tv_list)
        i = -1
        for url in self.iqiyi_tv_list:
            i += 1
            print "iqiyi", sum-i
            try:
                self.dive_into_iqiyi(url)
                time.sleep(2)
            except:
                print "[*] iqiyi PASS ", url
                open("outlog_video_pass.log", 'a').write("iqiyi\tpass\t%s\n" % url)
                if self.OUTLOG == OPEN:
                    self.undown_list.append(url)
                if self.OUTLOG == CLOSE:
                    self.log_list.append(url)
        print "[*] iqiyi down"
        open("all_video_log.log", 'a').write('[*]iqiyi down\n')

    def get_tv_info_from_tudou(self):
        sum = len(self.toudou_tv_list)
        i = -1
        for url in self.toudou_tv_list:
            i += 1
            print "tudou", sum-i
            try:
                self.dive_into_tudou(url)
                time.sleep(random.randint(1, 3))
            except:
                print "tudou PASS ", url
                open("outlog_video_pass.log", 'a').write("tudou\tpass\t%s\n" % url)
                if self.OUTLOG == OPEN:
                    self.undown_list.append(url)
                if self.OUTLOG == CLOSE:
                    self.log_list.append(url)
        print "[*] tudou down"
        open("all_video_log.log", 'a').write('[*]tudou down\n')

    def get_tv_info_from_qq(self):
        service_args = ['--load-images=false']
        self_driver = webdriver.PhantomJS(service_args=service_args)
        self_driver.set_page_load_timeout(6)

        sum = len(self.qq_tv_list)
        i = -1
        for url in self.qq_tv_list:
            i += 1
            print "qq", sum-i
            try:
                self.dive_into_qq(url, self_driver)
                time.sleep(2)
            except:
                print "[*]qq PASS ", url
                open("outlog_video_pass.log", 'a').write("qq\tpass\t%s\n" % url)
                if self.OUTLOG == OPEN:
                    self.undown_list.append(url)
                if self.OUTLOG == CLOSE:
                    self.log_list.append(url)
        print "[*] qq down"
        open("all_video_log.log", 'a').write('[*]qq down\n')

    def get_tv_info_from_wasu(self):
        sum = len(self.wasu_tv_list)
        i = -1
        for url in self.wasu_tv_list:
            i += 1
            print "wasu", sum-i
            try:
                self.dive_into_wasu(url)
                time.sleep(2)
            except Exception, e:
                print "[*]wasu PASS ", url
                print "e:\n", e
                open("outlog_video_pass.log", 'a').write("wasu\tpass\t%s\n" % url)
                if self.OUTLOG == OPEN:
                    self.undown_list.append(url)
                if self.OUTLOG == CLOSE:
                    self.log_list.append(url)
            # self.dive_into_wasu(url)
        print "[*] wasu down"
        open("all_video_log.log", 'a').write('[*]wasu down\n')
        open("outlog_video_down.log", 'a').write("[*]wasu down\n")

    def get_tv_info_from_tv_list(self):
        '''give the tv_list, get the tv_info'''
        threads = []
        t1 = threading.Thread(target=self.get_tv_info_from_youku)
        t2 = threading.Thread(target=self.get_tv_info_from_tudou)
        t3 = threading.Thread(target=self.get_tv_info_from_mangguo)
        t5 = threading.Thread(target=self.get_tv_info_from_leshi)
        t6 = threading.Thread(target=self.get_tv_info_from_iqiyi)
        t7 = threading.Thread(target=self.get_tv_info_from_qq)
        t8 = threading.Thread(target=self.get_tv_info_from_sohu)
        t9 = threading.Thread(target=self.get_tv_info_from_wasu)

        if self.youku_tv_list:
            print "[*]add youku"
            open("all_video_log.log", 'a').write('add youku\n')
            threads.append(t1)
        if self.souhu_tv_list:
            open("all_video_log.log", 'a').write('add sohu\n')
            print "[*]add sohu"
            threads.append(t8)
        if self.toudou_tv_list:
            open("all_video_log.log", 'a').write('add tudou\n')
            print "[*]add tudou"
            threads.append(t2)
        if self.mangguo_tv_list:
            open("all_video_log.log", 'a').write('add mangguo\n')
            print "[*]add mangguo"
            threads.append(t3)
        if self.leshi_tv_list:
            open("all_video_log.log", 'a').write('add letv\n')
            print "[*]add leshi"
            threads.append(t5)
        if self.iqiyi_tv_list:
            open("all_video_log.log", 'a').write('add iqiyi\n')
            print "[*]add iqiyi"
            threads.append(t6)
        if self.qq_tv_list:
            open("all_video_log.log", 'a').write('add qq\n')
            print "[*]add qq"
            threads.append(t7)
        if self.wasu_tv_list:
            open("all_video_log.log", 'a').write('add wasu\n')
            print "[*]add wasu"
            threads.append(t9)

        print "threads:", threads

        if len(threads) != 0:
            i = threads[0]
            for j in threads:
                i = j
                i.setDaemon(True)
                i.start()
                print "[***] %d" % threading.activeCount()
            for t in threads:
                print type(t), ".join"
                t.join()
        else:
            pass


def re_start(info):
    # reget from outlog_url
    if info.OUTLOG == CLOSE:
        info.OUTLOG = OPEN
        ii = CLOSE
        # url_list = ("outlog_video_pass.log").readlines()
        url_list = info.log_list
        print "::::::::::::::::::::::::::::::::::::"
        print "::::::::::::::::::::::::::::::::::::"
        print "log_list:", info.log_list
        print "::::::::::::::::::::::::::::::::::::"
        print "::::::::::::::::::::::::::::::::::::"
        if url_list:
            ii = OPEN
            open("all_video_log.log", 'a').write('FIRST EXCEPT -time:%s--\n' % time.ctime())
            for uu in url_list:
                open("all_video_log.log", 'a').write(uu+'\n')
            info.mix_list = url_list
            info.pick_up_tv_list_from_mix_list(info.mix_list)
            info.get_tv_info_from_tv_list()
            # data = open("outlog_video_pass.log")
        else:
            open("all_video_log.log", 'a').write('work well no except   -time:%s--\n' % time.ctime())


        print "::::::::::::::::::::::::::::::::::::"
        print "::::::::::::::::::::::::::::::::::::"
        print "undown_list:", info.undown_list
        print "::::::::::::::::::::::::::::::::::::"
        print "::::::::::::::::::::::::::::::::::::"
        if info.undown_list:
            open("all_video_log.log", 'a').write('UNDOWN --time:%s--\n' % time.ctime())
            for uu in info.undown_list:
                open("all_video_log.log", 'a').write(uu+'\n')
        if not info.undown_list and ii == OPEN:
            open("all_video_log.log", 'a').write('all UNDOWN repair --time:%s--\n' % time.ctime())

def start(tv_name):
    import copy
    sql = 'SELECT url FROM video WHERE 1'
    url_dir_list = do_db.query(sql)
    mixlist = []
    for url_dir in url_dir_list:
        mixlist.append(url_dir['url'])
    print "all urls", len(mixlist)
    info = Get_info_from_souku_and_ori_source(tv_name)
    print "old mix_list", len(info.mix_list)

    cpmix = copy.copy(info.mix_list)
    for url1 in cpmix:
        if url1 in mixlist:
            info.mix_list.remove(url1)

    # #  test
    # info.mix_list = [
    #     'http://v.qq.com/cover/8/8649jaib9idoaca.html',
    #                  ]
    print "new mix_list", len(info.mix_list)
    time.sleep(2)

    info.pick_up_tv_list_from_mix_list(info.mix_list)
    if info.mix_list:
        try:
            open("all_video_log.log", 'a').write('---------------------------------------------------------\n')
            open("all_video_log.log", 'a').write('-----------%s start----------------\n' % time.ctime())
            open("all_video_log.log", 'a').write('-----------video : %s-----------------------\n' % tv_name.encode('utf-8'))
            open("all_video_log.log", 'a').write('-----------count : %d-----------------------------\n' % info.url_count)
            open("all_video_log.log", 'a').write('---------------------------------------------------------\n')
            info.get_tv_info_from_tv_list()
        except Exception, e:
            print e
            open("all_video_log.log", 'a').write('FIRST EXCEPT THREAD BROKEN --%s--\n%s\n' % (time.ctime(), e))
        re_start(info)

    else:
        print "url or tv name none"
    info.log_list = []
    info.undown_list = []
    info.OUTLOG = CLOSE


if __name__ == '__main__':
    while(1):
        print type(time.ctime().split()[-2].split(':')[0])
        if time.ctime().split()[-2].split(':')[0] != '00':
            print "ko"
            sql = 'SELECT word FROM crawl_words WHERE 1'
            word_dir_list = do_db.query(sql)
            print "word_dir_list:", len(word_dir_list)
            for word_dir in word_dir_list:
                print "*************************************"
                print "**********%s*****************" % word_dir['word']
                print "*************************************"
                start(word_dir['word'])
            print "[*] all down"
        else:
            open("all_video_log.log", 'a').write('%s\n' % time.ctime())
            print "not this time"
            time.sleep(3600)
