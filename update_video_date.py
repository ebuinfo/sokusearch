#!/usr/bin/env python
# coding=utf-8

import time
import re
import toolkitv
from toolkitv import MySQLUtility


db = MySQLUtility('localhost', 'webinfosys', 'root', 'admin@edb')


def export_sentiment_words():
    sql = 'select * from word_list'
    data = db.query(sql)
    print 'data:', len(data)
    words = ['%s %s %s' % (d['word'], d['word_val'], d['word_property']) for d in data]
    with open('/ebunlp_data/z-sentiment-words-db.txt', 'w') as f:
        f.write('\n'.join(words))


def get_date(vname):
    mm = re.search(r'\d+', vname)
    if not mm:
        return 0
    return mm.group()


def get_name(url):
    _, html = toolkitv.downloader(url)
    if not html:
        return ''
    title = re.search(r'<title>(.*?)</title>', html)
    tts = []
    if title:
        title = title.groups()[0]
        if 'GBK' in html:
            title = title.decode('gbk').encode('utf8')
        zz = re.split(r'(?:_|-|—)', title)
        for i, z in enumerate(zz):
            z = z.strip()
            if not z:
                continue
            if i == 0:
                tts.append(z)
            elif '《' in z or '期' in z:
                tts.append(z)
    else:
        title = ''
    title = '-'.join(tts)
    return title


def crontab_update():
    idlog = toolkitv.IDLog('update_video_name.idlog')
    from_id = idlog.get_id()
    start = 138600
    if from_id < start:
        from_id = start
    from_id = update_video_name('', from_id=from_id)
    idlog.save_id(from_id)


def update_video_name(key, from_id=0):
    # sql = 'select id, url, video_name, video_date, url from video where video_name like "%%%%%s%%%%"' % key
    # sql = "select id, video_name, video_date, url from video where video_name like '%%%%三生三世%%%%' and url like '%%%%tudou.com%%%%'";
    sql = 'select id, url, video_name, video_date, url from video where id>%s and video_date=0 order by id asc' % from_id
    print sql
    data = db.query(sql)
    print 'data:', len(data)
    if not data:
        return from_id
    for d in data:
        # if d['video_date'] > 0:
        #     print 'skip:', d['id'], d['video_name'], d['video_date'], d['url']
        #     continue
        time.sleep(1)
        name = get_name(d['url'].encode('utf8'))
        print d['url'], name
        if not name or '404' in name:
            print 'bad name:', d['id'], d['url']
            continue
        ji = get_date(name)
        if not ji:
            print '\tno ji:', name, d['id'], d['url']
            continue
        sql = 'update video set video_name="%s", video_date=%s where id=%s' % (name, ji, d['id'])
        print sql
        db.execute(sql)
    return data[-1]['id']


def update():
    sql = 'select id, video_name, video_date from video where video_date=0'
    data = db.query(sql)
    print 'data:', len(data)
    for d in data:
        date = get_date(d['video_name'])
        if not date:
            continue
        print d['video_name'], '->', date, d['id']
        sql = 'update video set video_date=%s where id=%s' % (date, d['id'])
        db.execute(sql)
        # break
    print 'done'


if __name__ == '__main__':
    from sys import argv
    opt = argv[1]
    if opt == 'name':
        key = '三生三世'
        update_video_name(key)
    elif opt == 'date':
        update()
    elif opt == 'crontab':
        crontab_update()
    else:
        print 'invalid option:', opt
