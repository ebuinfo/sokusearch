#!/usr/bin/env python
#coding:utf8

import re
import time
import requests
import torndb
import string
import traceback

CLI_RED = '\x1b[31m'
CLI_GRE = '\x1b[32m'
CLI_BRO = '\x1b[33m'
CLI_BLU = '\x1b[34m'
CLI_PUR = '\x1b[35m'
CLI_CYA = '\x1b[36m'
CLI_WHI = '\x1b[37m'
CLI_NOR = '\x1b[0m'



class MySQLUtility(object):
    '''extended utility to torndb
       conn is an instance of torndb.Connection
    '''
    def __init__(self, host, database, user, passwd):
        self.conn = torndb.Connection(
            host,
            database,
            user,
            passwd
        )
        ## torndb set tz to '0:00', but all other programme use 'SYSTEM'
        ## so,
        self.conn.execute('set time_zone = "SYSTEM"')
        self.conn.execute('SET NAMES utf8mb4')
        #sql = 'show variables like "%%time_zone%%"'
        #tz = self.conn.query(sql)
        #print 'MySQL time_zone:', tz

    def get(self, query, *parameters):
        '''wrapper to self.conn.get()'''
        return self.conn.get(query, *parameters)

    def query(self, query, *parameters):
        return self.conn.query(query, *parameters)

    def execute(self, query, *parameters):
        try:
            last_id = self.conn.execute(query, *parameters)
            return last_id
        except Exception, e:
            #traceback.print_exc()
            if e[0] == 1062: # just skip duplicated item
                pass
            elif e[0] == 2006: # mysql has gone way
                err = e[0]
                while err == 2006:
                    print 'MySQL has gone awy, try to reconnect in 5 secs'
                    time.sleep(5)
                    try:
                        self.conn.reconnect()
                        last_id = self.conn.execute(query, *parameters)
                        return last_id
                    except Exception, e:
                        err = e[0]
            else:
                print 'Exception:', e
                print 'sql:', query
                print 'values:', parameters
                print 'item:'
                raise e


    def is_in_table(self, table_name, field, value):
        sql = 'SELECT %s FROM %s WHERE %s="%s"' % (field, table_name, field, value)
        d = self.conn.get(sql)
        if d: return True
        return False

    def update_table(self, table_name, updates,
                     field_where, value_where):
        '''updates is a dict of {field_update:value_update}'''
        sets = []
        for k in updates.keys():
            s = '%s=%%s' % k
            sets.append(s)
        sets = ','.join(sets)
        sql = 'UPDATE %s SET %s WHERE %s="%s"' % (
            table_name,
            sets,
            field_where, value_where,
        )
        try:
            self.conn.execute(sql, *(updates.values()))
        except Exception, e:
            print e
            for k in updates:
                print k, updates[k]

    def item_to_table(self, table_name, item):
        '''item if a dict : key is mysql table field'''
        fields = ','.join(item.keys())
        valstr = ','.join(['%s'] * len(item))
        sql = 'INSERT INTO %s (%s) VALUES(%s)' % (table_name, fields, valstr)
        try:
            last_id = self.conn.execute(sql, *(item.values()))
            return last_id
        except Exception, e:
            #traceback.print_exc()
            if e[0] == 1062: # just skip duplicated item
                pass
            else:
                print 'Exception:', e
                print 'sql:', sql
                print 'values:', item.values()
                print 'item:'
                for k,v in item.items():
                    print k, ' : ', v
                raise e

    def items_to_table(self, table_name, items):
        '''insert multi-item to the table
            items is a list of dict
        '''
        if not items:return
        fields = ','.join(items[0].keys())
        valstr = ','.join(['%s'] * len(items[0]))
        values = [item.values() for item in items]

        sql = 'INSERT INTO %s (%s) VALUES(%s)' % (table_name, fields, valstr)
        self.conn.executemany(sql, values)


class IDLog(object):
    '''save & read ID to/from a file
    '''
    def __init__(self, filename):
        self._fn = filename

    def save_id(self, _id):
        f = open(self._fn, 'w')
        f.write(str(_id))
        f.close()

    def get_id(self,):
        _id = 0
        try:
            s = open(self._fn).read(32).strip()
            _id = int(s)
        except:
            pass
        return _id

def downloader(url, timeout=10, refer=''):
    header = {
        'User-Agent': ('Mozilla/5.0 (compatible; MSIE 9.0; '
                       'Windows NT 6.1; Win64; x64; Trident/5.0)'),
    }
    if refer:
        header['Referer'] = refer
    status_code = 401
    try:
        r = requests.get(url, headers=header, timeout=timeout)
        html = r.content
        status_code = r.status_code
    except:
        traceback.print_exc()
        msg = 'failed new:'+ url + '\n'
        print msg
        html = ''
    return (status_code, html)

def is_punc(word):
    if word in string.punctuation:
        return True
    uch = word.decode('utf8')
    uch = ord(uch[0])
    if 0x4E00 <= uch and uch <= 0x9FCF:
        return False
    return True


p_rmTag = re.compile(r'<[^>]*>', re.DOTALL)
def remove_html_tag(html):
    return p_rmTag.sub('', html)


def init_file_logger(fname):
    ## config logging
    import logging
    from logging.handlers import TimedRotatingFileHandler
    ch = TimedRotatingFileHandler(fname, when="midnight")
    ch.setLevel(logging.INFO)
    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # add formatter to ch
    ch.setFormatter(formatter)
    logger = logging.getLogger(__name__)
    # add ch to logger
    logger.addHandler(ch)
    return logger



if __name__ == '__main__':
    # tu = MySQLUtility('localhost', 'test', 'root', 'root@db')
    # g = tu.is_in_table('simple', 'id', 3)
    # print g
    # updates = {
    #     'title': 'by_"update"_table()',
    #     'text': 'by_update_table()',
    #     'v': 123,
    # }
    # tu.update_table('simple', updates, 'id', 1)

    # new_item = {
    #     'title':'item_to_table',
    #     'text':'item_to_table',
    # }
    # tu.item_to_table('simple', new_item)

    # items = [
    #     {
    #         'title':'items_to_table_1',
    #         'text':'items_to_table_1',
    #     },
    #     {
    #         'title':'items_to_table_2',
    #         'text':'items_to_table_2',
    #     }
    # ]
    # tu.items_to_table('simple', items)
    # print 'test succeed!'

    url = 'http://k.youku.com/player/getFlvPath/sid/141525658275317537210_00/st/flv/fileid/0300020100544E1D9FF8EF003E88036A9F76B4-F9CF-993B-C34D-7330C01BEF4A?K=2add4a33e3b5432b261dfe42&hd=0&myp=0&ts=144&ymovie=1&ypp=0&ct'
    refer = 'http://static.youku.com/v1.0.0481/v/swf/loader.swf'
    _, html = downloader(url, refer=refer)
    print _
    print len(html)
    open('z', 'w').write(html)

