#!/usr/bin/python
# encoding=utf8


import requests
import re
import time


def get_real_num(a):
    if type(a) == int:
        return a
    b = a.encode("utf-8").split(',')
    if len(b) == 4:
        d = int(b[0]) * 1000000000 + int(b[1]) * 1000000 + int(b[2]) * 1000 + int(b[3])
    if len(b) == 3:
        d = int(b[0]) * 1000000 + int(b[1]) * 1000 + int(b[2])
    if len(b) == 2:
        d = int(b[0]) * 1000 + int(b[1])
    if len(b) == 1:
        d = int(b[0])
    return d


def dive_into_sohu(url):
    if 'v.tv.sohu' in url:
        return
    r = requests.get(url, timeout=20)
    time.sleep(1)
    if r.status_code == 404:
        return
    # print url

    id_html = r.text
    reg = r'var vid\=\"(.*?)\"'
    id = re.search(reg, id_html).groups()[0]
    reg = r'var playlistId\=\"(\d*?)\"'
    playlistid = re.search(reg, id_html).groups()[0]
    if playlistid == '': playlistid = str(0)
    reg = r'var cid\=\"(.*?)\"'
    typeid = re.search(reg, id_html).groups()[0]
    if typeid == '': typeid = str(0)

    url_pre = 'http://score.my.tv.sohu.com/digg/get.do?vid=%s&type=%s'
    url_pre %= (id, typeid)
    html = requests.get(url_pre, timeout=20).text

    # get up / down
    reg = r'upCount\"\:(\d*?)\,'
    up = re.search(reg, html).groups()[0]
    up = get_real_num(up)
    reg = r'downCount\"\:(\d*?)\}'
    down = re.search(reg, html).groups()[0]
    down = get_real_num(down)

    url_pre = 'http://count.vrs.sohu.com/count/queryext.action?vids=%s'
    url_pre %= id
    html = requests.get(url_pre, timeout=20).text

    # get play
    reg = 'total\"\:(\d*?)\,'
    play = re.search(reg, html).groups()[0]
    play = get_real_num(play)
    # print play

    url_pre = 'http://access.tv.sohu.com/reply/list/%s_%s_%s_0_10.js'
    url_pre %= (typeid, playlistid, id)
    html = requests.get(url_pre, timeout=20).text

    # get comment
    reg = r'allCount\"\:(.*?)\,'
    comment = re.search(reg, html).groups()[0]
    comment = get_real_num(comment)

    rt = {'url':url, 'play':int(play), 'comment':int(comment), 'up':int(up), 'down':int(down)}
    # print str(rt)
    return rt


if __name__ == '__main__':
    url = 'http://tv.sohu.com/20160218/n437802409.shtml'
    dive_into_sohu(url)

