#!/usr/bin/python
# coding=utf-8


from gevent import monkey; monkey.patch_all()
import gevent
from selenium import webdriver
import urllib
import re
from toolkitv import MySQLUtility
import fnvhash
import time
import threading
import requests
from db_info import db
import datetime


class GetInfoFromSoku(object):
    def __init__(self):
        self.mang_dir = {}
        self.tudou_tv_list = []
        self.youku_tv_list = []
        self.mangguo_tv_list = []
        self.leshi_tv_list = []
        self.qq_tv_list = []
        self.sohu_tv_list = []
        self.mangguo_tv_info_dir = {}
        self.youku_tv_info_dir = {}
        self.tudou_tv_info_dir = {}
        self.kankan_tv_list = []
        self.pps_tv_list = []
        self.iqiyi_tv_list = []
        self.wasu_tv_list = []
        self.mix_list = []
        self.do_db = db['self']

        # set the monitor time limit in 90 days
        pre_date = (datetime.datetime.now() - datetime.timedelta(days = 90)).strftime("%Y%m%d")
        sql = 'SELECT url FROM video where video_date > %s' % pre_date
        # sql = 'SELECT url FROM video where DATE_SUB(CURDATE(), INTERVAL 90 DAY) <= date(create_time )'
        self.url_dir_list = self.do_db.query(sql)
        print "len(url)", len(self.url_dir_list)
        # for url_dir in self.url_dir_list:
        #     self.mix_list.append(url_dir['url'])
        self.mix_list = [i['url'] for i in self.url_dir_list]

    def pick_up_tv_list_from_mix_list(self):
        self.tudou_tv_list = []
        self.youku_tv_list = []
        self.mangguo_tv_list = []
        self.sohu_tv_list = []
        self.iqiyi_tv_list = []
        self.kankan_tv_list = []
        self.qq_tv_list = []
        self.leshi_tv_list = []
        for http in self.mix_list:
            if 'www.tudou.com' in http:
                self.tudou_tv_list.append(http)
            if 'v.youku.com' in http:
                self.youku_tv_list.append(http)
            if 'www.hunantv.com' in http:
                self.mangguo_tv_list.append(http)
            if 'tv.sohu.com' in http:
                self.sohu_tv_list.append(http)
            if 'www.letv.com' in http:
                self.leshi_tv_list.append(http)
            if 'v.qq.com' in http:
                self.qq_tv_list.append(http)
            if 'vod.kankan.com' in http:
                self.kankan_tv_list.append(http)
            if 'pps.tv' in http:
                self.pps_tv_list.append(http)
            if 'iqiyi.com' in http:
                self.iqiyi_tv_list.append(http)
            if 'wasu.cn' in http:
                self.wasu_tv_list.append(http)
        if len(self.iqiyi_tv_list):
            print "iqiyi:"
            print len(self.iqiyi_tv_list)
        if len(self.tudou_tv_list):
            print "tudou:"
            print len(self.tudou_tv_list)
        if len(self.youku_tv_list):
            print "youku:"
            print len(self.youku_tv_list)
        if len(self.mangguo_tv_list):
            print "mangguo:"
            print len(self.mangguo_tv_list)
        if len(self.sohu_tv_list):
            print "sohu:"
            print len(self.sohu_tv_list)
        if len(self.leshi_tv_list):
            print "letv:"
            print len(self.leshi_tv_list)
        if len(self.qq_tv_list):
            print "qq:"
            print len(self.qq_tv_list)
        if len(self.wasu_tv_list):
            print "wasu:"
            print len(self.wasu_tv_list)

    def _get_kid(self, url):
        sql = "SELECT kid FROM video WHERE url='%s'" % url
        # print sql
        kid = self.do_db.query(sql)
        # print "kid", kid
        kid = kid[0]['kid']
        return kid

    def _get_site_url(self, url):
        site_url = ['v.youku.com',
                'tudou.com',
                'sohu.com',
                'iqiyi.com',
                'hunantv.com',
                'sohu.com',
                ]
        for u in site_url:
            if u in url:
                return u

    def _get_site_name_hash(self, url):
        site_url = self._get_site_url(url)
        if site_url == 'v.youku.com': site_name = '优酷'
        if site_url == 'tudou.com': site_name = '土豆'
        if site_url == 'iqiyi.com': site_name = '爱奇艺'
        if site_url == 'hunantv.com': site_name = '芒果'
        if site_url == 'sohu.com': site_name = '搜狐'
        return fnvhash.fnv_32a_str(site_name)

    def push_into_db(self, info):
        import datetime

        info_dir_vedio_data = {}
        print "url->", info['url']
        info_dir_vedio_data['url_hash'] = fnvhash.fnv_64a_str(info['url'])
        print "hash->", info_dir_vedio_data['url_hash']

        info_dir_vedio_data['views'] = info['play']
        info_dir_vedio_data['good'] = info['up']
        if info['down'] != None:
            info_dir_vedio_data['bad'] = info['down']
        info_dir_vedio_data['kid'] = self._get_kid(info['url'])
        info_dir_vedio_data['site_hash'] = self._get_site_name_hash(info['url'])
        if info['comment'] != None:
            info_dir_vedio_data['comment_num'] = info['comment']
        info_dir_vedio_data['create_at'] = str(datetime.date.today() - datetime.timedelta(days=1))

        self.do_db.item_to_table('video_data', info_dir_vedio_data)

    def crawl_youku(self):
        from dive_into_youku import dive_into_youku
        for u in self.youku_tv_list:
            try:
                info = dive_into_youku(u)
                self.push_into_db(info)
                print "push youku"
            except Exception, e:
                print e

    def crawl_tudou(self):
        from dive_into_tudou import dive_into_tudou
        for u in self.tudou_tv_list:
            try:
                info = dive_into_tudou(u)
                self.push_into_db(info)
                print "push tudou"
            except Exception, e:
                print e

    def crawl_iqiyi(self):
        from dive_into_iqiyi import dive_into_iqiyi
        for u in self.iqiyi_tv_list:
            try:
                info = dive_into_iqiyi(u)
                self.push_into_db(info)
                print "push iqiyi"
            except Exception, e:
                print e

    def crawl_mangguo(self):
        from dive_into_mangguo import dive_into_mangguo
        for u in self.mangguo_tv_list:
            try:
                info = dive_into_mangguo(u)
                self.push_into_db(info)
                print "push mangguo"
            except Exception, e:
                print e

    def crawl_sohu(self):
        from dive_into_sohu import dive_into_sohu
        for u in self.sohu_tv_list:
            try:
                info = dive_into_sohu(u)
                self.push_into_db(info)
                print "push sohu"
            except Exception, e:
                print e

    def start(self):
        self.pick_up_tv_list_from_mix_list()

        gevent.joinall([
            gevent.spawn(self.crawl_youku),
            gevent.spawn(self.crawl_tudou),
            gevent.spawn(self.crawl_iqiyi),
            gevent.spawn(self.crawl_mangguo),
            gevent.spawn(self.crawl_sohu),
            ])


def debug(tv_site):
    gt = GetInfoFromSoku()
    gt.pick_up_tv_list_from_mix_list()

    if tv_site == 'sohu':
        gevent.joinall([ gevent.spawn(gt.crawl_sohu), ])

    if tv_site == 'iqiyi':
        gevent.joinall([ gevent.spawn(gt.crawl_iqiyi), ])

    if tv_site == 'mangguo':
        gevent.joinall([ gevent.spawn(gt.crawl_mangguo), ])

    if tv_site == 'youku':
        # gt.youku_tv_list = ['http://v.youku.com/v_show/id_XMTM3Mjk0ODMyNA==.html']
        gt.youku_tv_list = [
                'http://v.youku.com/v_show/id_XMTQ2MDMwMDczMg==.html',
                'http://v.youku.com/v_show/id_XMTQzODY4Mzg1Mg==.html',
                'http://v.youku.com/v_show/id_XMTQ0NTczMTI2MA==.html']
        for i in gt.youku_tv_list:
            # print i
            pass
        gevent.joinall([ gevent.spawn(gt.crawl_youku), ])

    if tv_site == 'tudou':
        gevent.joinall([ gevent.spawn(gt.crawl_tudou), ])

def start():
    gt = GetInfoFromSoku()
    gt.start()

def mession_loop():
    flag = 1
    while(1):
        if flag and int(time.ctime().split()[-2].split(':')[0]) == 13:
            print "ko"
            start()
            flag = 0
        else:
            flag = 1
            print "not this time and will sleep to 1h"
            time.sleep(3600)

if __name__ == '__main__':
    from sys import argv

    if len(argv) == 1:
        mession_loop()
    else:
        debug(argv[1])
