#!/usr/bin/python
# encoding=utf8


import requests
import re
import time


def dive_into_iqiyi(url):
    # print url
    r = requests.get(url, timeout=4)
    # if r.status_code == 404:
    #     return

    time.sleep(2)
    # get vid
    id_html = r.text
    # with open('aa.html', 'w') as f:
    #     f.write(id_html.encode('utf8'))
    reg = 'tvId\:(.*?)\,'
    vid = int(re.search(reg, id_html).groups()[0])

    pre_url = 'http://mixer.video.iqiyi.com/jp/recommend/videos?referenceId=%s&withRefer=true&type=video'
    pre_url %= str(vid)
    r = requests.get(pre_url, timeout=4)
    id_html = r.text
    # get play up down comm
    reg = r'%s[\w\W]*?%s[\w\W]*?%s' % (vid, vid, vid)
    temp = re.search(reg, id_html).group()
    reg = r'upCount\"\:(.*?)\,'
    up = re.search(reg, temp).groups()[0]
    reg = r'downCount\"\:(.*?)\,'
    down = re.search(reg, temp).groups()[0]
    reg = r'playCount"\:(.*?)\,'
    play = re.search(reg, temp).groups()[0]
    reg = r'commentCount"\:(.*?)\,'
    comment = re.search(reg, temp).groups()[0]

    return {'url':url, 'play':play, 'comment':comment, 'up':up, 'down':down}
