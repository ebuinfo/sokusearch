#!/usr/bin/env python
# coding: utf8

import re
import json
import sqlite3
import urllib
import requests
import time

from push_db import push_comment

DB = sqlite3.connect('SohuTopicid.db')
DBHandler = DB.cursor()
try:
    DBHandler.execute(
    '''CREATE TABLE sohu_topic(vid INTEGER, topic_id INTEGER)
    ''')
except sqlite3.OperationalError:
    pass

Gsession = requests.Session()
Gsession.headers.update(
    {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
    })

def redict_url(vid, cid, pid, playlistId):
    base = "http://tv.sohu.com/upload/static/movieCommit/remark.html?"
    q = {
        #'cid': "2",
        'cid': str(cid),
        'vid': str(vid), #"3323578"
        'uid': "",
        #'pid': "467750710",
        'pid': str(pid),
        'playlistId': str(playlistId), #"9174927"
    }
    query = urllib.urlencode(q)
    return base+query

def first_comment_url(vid, cid, pid, playlistId, client_id=""):
    base = 'http://changyan.sohu.com/api/2/topic/load?'
    if not client_id:
        client_id="cyqyBluaj"
    url = redict_url(vid, cid, pid, playlistId)
    q = [
        ('client_id', client_id),
        ('topic_source_id', str(vid)),
        ('page_size', '30'),
        ('order_by', 'time'),
        ('topic_url', url),
    ]
    query = urllib.urlencode(q)
    return base+query

def common_comment_url(topid, client_id="", page=2):
    base = 'http://changyan.sohu.com/api/2/topic/comments?'
    if not client_id:
        client_id="cyqyBluaj"
    q = [
        ('client_id', client_id),
        ('topic_id', str(topid)),
        ('page_no', str(page)),
        ('page_size', '30'),
        ('order_by', 'time'),
    ]
    query = urllib.urlencode(q)
    return base+query

def save_topic_id(vid, topid):
    global DB, DBHandler
    DBHandler.execute('''INSERT INTO sohu_topic(vid, topic_id)
    values (?, ?)''', (int(vid), int(topid)))
    DB.commit()

def query_topic_id(vid):
    global DB, DBHandler
    DBHandler.execute('''SELECT topic_id FROM sohu_topic
    WHERE vid=?''', (int(vid),))
    rows = DBHandler.fetchall()
    if not rows:
        raise Exception('cannot find topic_id')
    return rows[0][0]

def get_sohu_comments(vid, cid='', pid='', playlistId='', page=1):
    global Gsession
    if page <= 1:
        url = first_comment_url(vid, cid, pid, playlistId)
    else:
        topid = query_topic_id(vid)
        url = common_comment_url(topid, page=page)
    l = []
    try:
        print '[ GET ]', url
        r = Gsession.get(url)
        time.sleep(1)
        if r.status_code == 200:
            d = json.loads(r.text)
            comments = d['comments']
            for c in comments:
                dd = {}
                dd['content'] = c['content'].encode('utf8')
                dd['pubtime'] = int(c['create_time'])/1000
                # print '.',
                l.append(dd)
            if page <= 1:
                save_topic_id(vid, d['topic_id'])
    except:
        pass
    finally:
        return l

def get_comment(source_url):
    cid = 0
    pid = 0
    playlistId = 0
    vid = 0
    html = ''
    i = 2
    while i >= 1:
        try:
            r = requests.get(source_url, headers={
'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
            })
            time.sleep(1)
            if r.status_code == 200:
                html = r.content
                break
        except:
            i -= 1
    if html != '':
        re_vid = re.compile('.* var vid="(\d+)"', re.S)
        re_pid = re.compile('.* var pid="(\d+)"', re.S)
        re_cid = re.compile('.* var cid="(\d+)"', re.S)
        re_plid = re.compile('.* var playlistId="(\d+)"', re.S)
        m = re.match(re_vid, html)
        if m:
            vid = m.groups()[0]
            print '['
            print 'source url:', source_url
            print 'vid:', vid
            print ']'
        m = re.match(re_pid, html)
        if m:
            print 'find pid'
            pid = m.groups()[0]
        m = re.match(re_cid, html)
        if m:
            print 'find cid'
            cid = m.groups()[0]
        m = re.match(re_plid, html)
        if m:
            print 'find playlistId'
            playlistId = m.groups()[0]
        for x in range(1, 500):
            if x <= 1:
                ll = get_sohu_comments(vid, cid, pid, playlistId, page=1)
            else:
                ll = get_sohu_comments(vid, page=x)
            s = push_comment(ll, source_url)

            if s < 1:
                return


def main(url):
    get_comment(url)



if __name__ == '__main__':
    # vid = 3323578
    # ll = get_sohu_comments(vid, page=2)
    # for d in ll:
    #     for k in d:
    #         print k, ":", d[k]

    url = 'http://tv.sohu.com/20161011/n470026513.shtml '
    main(url)
