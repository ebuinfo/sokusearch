#!/usr/bin/python
#encoding=utf8


import requests
import time
import re
import json
from push_db import push_comment


def main(video_url):
    get_comment(video_url)


def get_comment(video_url):
    print video_url
    '''param:
        * page: special(manaul)
        * page_size: fixed(less than 200)
        * reply_sort: fixed('hot' or 'add_time')
        * sort: fixed('hot' or 'add_time')
        * tvid: videoid
        '''
    tvid = get_vid(video_url)
    ori_url = 'http://api.t.iqiyi.com/qx_api/comment/get_video_comments?page=%s&page_size=200&reply_sort=add_time&sort=add_time&tvid=%s' % (1, tvid)
    print ori_url

    for i in range(1, 500):
        print '&&& page %s&&&' % (i)
        p = get_onepage_comment_list(ori_url.replace("page=1", "page=%s"%i))
        s = push_comment(p, video_url)
        if s < 1:
            return


def get_vid(url):
    r = requests.get(url)
    time.sleep(1)
    reg = 'tvId\:(.*?)\,'
    vid = int(re.search(reg, r.content).groups()[0])
    return vid
    # with open('zz.html', 'w') as f:
    #     f.write(r.content)


def get_onepage_comment_list(url):
    r = requests.get(url)
    time.sleep(1)
    # with open('zzg.html', 'w') as f:
    #     f.write(r.content)
    d = parse(r.content)
    return d


def parse(content):
    a = json.loads(content)
    # print a.keys()
    if a["code"] != u'A00000':
        print a["code"]
        print "iqiyi parse json with unsuccess!"
        return {}
        # exit()
    comments = a["data"]["comments"]
    ds = []
    for c in comments:
        d = {}
        d["content"] = c["content"]
        d["pubtime"] = c["addTime"]
        ds.append(d)
    return ds

if __name__ == '__main__':
    vrl = 'http://www.iqiyi.com/v_19rr9convg.html'
    main(vrl)
