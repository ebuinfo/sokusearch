#!/usr/bin/python
# encoding=utf8


import requests
import json
import time
import sys
sys.path.append('../')
import fnvhash
import re
from comment_config import db
from sentiment_weibo import get_sentiment_danmu
# db = MySQLUtility('localhost', 'video', 'root', 'root')


def filter_emoji(desstr,restr=''):
    try:
        co = re.compile(u'[\U00010000-\U0010ffff]')
    except re.error:
        co = re.compile(u'[\uD800-\uDBFF][\uDC00-\uDFFF]')
    return co.sub(restr, desstr)


def push_comment(result, url):
    i = 0
    j = 0

    if len(result) == 0:
        return 0

    url_hash = fnvhash.fnv_64a_str(url)
    for d in result:
        d['url_hash'] = url_hash
        if type(d['content']) == str:
            d['content'] = d['content'].decode('utf8')
        d['content'] = filter_emoji(d['content'])
        d['content'] = d['content'].encode('utf8')
        if type(d['content']) != str:
            print '!!!!! ==== not str content:', d['content']
            continue
        d['content_hash'] = fnvhash.fnv_64a_str(d['content'])
        d['sentiment'] = get_sentiment_danmu(d['content'])
        dc = int(d['pubtime'])
        if dc > 10000000000:
            d['pubtime'] = dc/1000
        else:
            d['pubtime'] = dc
        _id = db.item_to_table('video_comment', d)
        if _id:
            print '+',
            sys.stdout.flush()
            i += 1
            j = 0
        else:
            print '.',
            sys.stdout.flush()
            # print d['content']
            # print d['content_hash']
            j += 1
            if j >= 15:
                print i
                return -1
    print i
    return i

