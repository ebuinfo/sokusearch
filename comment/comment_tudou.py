#!/usr/bin/python
#encoding=utf8


import requests
import time
import re
import json
from push_db import push_comment


def main(video_url):
    get_comment(video_url)


def get_comment(video_url):
    print video_url
    '''param:
        * page: special(manaul)
        * page_size: fixed(less than 200)
        * reply_sort: fixed('hot' or 'add_time')
        * sort: fixed('hot' or 'add_time')
        * tvid: videoid
        '''
    try:
        tvid = get_vid(video_url)
    except:
        print 'tudou get vid error'
    ori_url = 'http://www.tudou.com/comments/itemnewcomment.srv?iid=%s&page=%s&rows=150&method=getCmt&charset=utf-8' % (tvid, 1)
    # get_onepage_comment_list(ori_url)
    list_cnt = 500
    print ori_url

    for i in range(1, list_cnt):
        uu = ori_url.replace("page=1", "page=%s"%i)
        print '***%s***' % (uu)

        p = get_onepage_comment_list(uu)
        s = push_comment(p, video_url)
        if s < 1:
            return


def get_vid(url):
    r = requests.get(url)
    time.sleep(1)
    # with open('tt.html', 'w') as f:
        # f.write(r.content)
    rreg = r',iid: (.*?)[^\d]'
    iid = re.search(rreg, r.text).groups()[0]
    return iid


def get_onepage_comment_list(url):
    r = requests.get(url)
    time.sleep(1)
    # with open('zzg.html', 'w') as f:
        # f.write(r.content)
    d = parse(r.content)
    return d


def parse(content):
    a = json.loads(content)
    # print a.keys()
    comments = a["data"]
    ds = []
    for c in comments:
        d = {}
        d["content"] = c["content"]
        d["pubtime"] = c["publish_time"]
        ds.append(d)
    return ds


if __name__ == '__main__':
    vrl = 'http://www.tudou.com/albumplay/s866liBTy6I/GkyOBARXADE.html'
    vrl = 'http://www.tudou.com/albumplay/VEJMkuTrP_A/_RQNGV4Tefg.html'
    main(vrl)
