#!/usr/bin/python
#encoding=utf8


import traceback
import requests
import time
import datetime
import re
from push_db import push_comment
import fnvhash
from comment_config import db
import json
import urllib


def get_comment(video_url, vid=''):
    print video_url
    '''param:
        * jsoncallback: fixed
        * objectId:     videoid
        * app:          fixed
        * currentPage:  special(manaul)
        * pageSize:     fixed(30)
        * listType:     fixed()
        * time:         special(time.time())
        * sign:         special(with arg time)
        '''
    time_ = str(int(time.time()*1000))
    app = '100-DDwODVkv'
    sign = make_sign(time_, app)
    if not vid:
        objectId = get_vid(video_url)
    else:
        objectId = vid
    if not objectId:
        print 'no videoId got for:', video_url
        return

    purl = 'http://p.comments.youku.com/ycp/comment/pc/commentList?'
    query = [
        ('objectId', objectId),
        ('app', app),
        ('currentPage', 1),
        ('pageSize', 30),
        ('listType', 0),
        ('sign', sign),
        ('time', time_),
    ]
    ori_url = purl + urllib.urlencode(query)
    print ori_url

    for i in range(1, 500):
        print '***yk page %s***' % (i)
        try:
            p = get_onepage_comment_list(ori_url.replace("Page=1", "Page=%s"%i))
            print 'get comments:', len(p)
        except:
            print 'exception!!!!!!!!'
            traceback.print_exc()
            break
        s = push_comment(p, video_url)
        '''
        s :
            -1: has repeat more than 15 comment
            -2: wrong
            0: has no comment
            >1: comment count
        '''
        if s < 1:
            break


def make_sign(time_, app):
    r = app
    c = "6c4aa6af6560efff5df3c16c704b49f1"
    l = time_
    m = r + '&' + c + '&'+ l
    return md5(m)


def get_vid(url):
    r = requests.get(url)
    # time.sleep(1)
    pp = [
        'videoId:\"(.*?)\"',
    ]
    vid = '0'
    for p in pp:
        mm = re.search(p, r.content)
        if mm:
            vid = mm.groups()[0]
            break
    print "vid", vid
    vid = int(vid)
    if vid:
        url_hash = fnvhash.fnv_64a_str(url)
        sql = 'update video set vid="%s" where url_hash=%s' % (vid, url_hash)
        print sql
        db.execute(sql)
    return vid


def md5(e):
    import hashlib
    m = hashlib.md5()
    m.update(e)
    return m.hexdigest()


def get_onepage_comment_list(url):
    r = requests.get(url)
    # with open('zz.html', 'w') as f:
        # f.write(r.content)
    time.sleep(1)
    d = parse(r.content)
    return d


def parse(content):
    a = json.loads(content)
    # print a.keys()
    # if a["message"] != u'success':
    #     print a["message"]
    #     print "parse json with unsuccess!"
    comments = a["data"]["comment"]
    ds = []
    for c in comments:
        d = {}
        d["content"] = c["content"]
        d["pubtime"] = c["createTime"]
        ds.append(d)
    return ds


def main():
    # set the monitor time limit in 30 days
    pre_date = (datetime.date.today() - datetime.timedelta(days = 30))
    pre_date = pre_date.strftime("%Y-%m-%d %H:%M:%S")
    print pre_date
    sql = ('SELECT url, vid, video_name FROM video where url like "%%%%v.youku.com%%%%" '
            'and create_time > \'%s\' order by id desc') % pre_date
    print sql
    data = db.query(sql)
    print "len(url)", len(data)
    time.sleep(2)
    for d in data:
        if u'预告' in d['video_name']:
            print 'skip:', d['video_name'], d['url']
            continue
        print 'getting comment:', d['video_name'], d['url']
        get_comment(d['url'], d['vid'])
        time.sleep(2)


def loop():
    while 1:
        main()
        time.sleep(5)


if __name__ == '__main__':
    from sys import argv
    if len(argv) > 1:
        # video_url = 'http://v.youku.com/v_show/id_XMTg0MTIyNzAxMg==.html'
        # video_url = 'http://v.youku.com/v_show/id_XMjY3OTk4NzM2NA==.html'
        get_comment(video_url)
    else:
        # main()
        loop()
