#!/usr/bin/python
# encoding=utf8


import time
import sys
sys.path.append('../')

from comment_config import db
from sentiment_weibo import get_sentiment_danmu


def get_data(from_id, limit):
    sql = 'select id, content, sentiment from video_comment where id>%s order by id limit %s' % (from_id, limit)
    return db.query(sql)


def main():
    from_id = 0
    limit = 100
    maxid = 33759894
    while 1:
        if from_id % 100 == 0:
            print 'from_id:', from_id
        data = get_data(from_id, limit)
        if not data:
            print 'no data'
            break
        for d in data:
            senti = get_sentiment_danmu(d['content'])
            if senti == d['sentiment']:
                continue
            sql = 'update video_comment set sentiment=%s where id=%s' % (senti, d['id'])
            print sql
            db.execute(sql)
        from_id = data[-1]['id']


if __name__ == '__main__':
    main()
