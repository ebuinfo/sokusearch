#!/usr/bin/python
#encoding=utf8


import requests
import time
import re
import json
from push_db import push_comment


def main(video_url):
    get_comment(video_url)


def get_comment(video_url):
    '''param:
        * page: special(manaul)
        * page_size: fixed(less than 200)
        * reply_sort: fixed('hot' or 'add_time')
        * sort: fixed('hot' or 'add_time')
        * tvid: videoid
        '''
    try:
        tvid = get_vid(video_url)
    except:
        print 'mgtv get vid error'
        exit()
    ori_url = 'http://comment.mgtv.com/video_comment/list/?subject_id=%s&page=%s' % (tvid, 1)
    print ori_url

    for i in range(1, 500):
        uu = ori_url.replace("page=1", "page=%s"%i)
        # print 'comment url', uu
        p = get_onepage_comment_list(uu)
        s = push_comment(p, video_url)
        if s < 1:
            return


def get_vid(url):
    try:
        vid = re.search(r'f\/(.*?).html', url).groups()[0]
        return vid
    except:
        pass
    # with open('mg.html', 'w') as f:
    #     f.write(r.content)
    r = requests.get(url)
    time.sleep(1)
    reg = 'vid\: (.*?)\,'
    vid = int(re.search(reg, r.content).groups()[0])
    return vid


def get_onepage_comment_list(url):
    print '----', url
    r = requests.get(url)
    time.sleep(1)
    # with open('zzg.html', 'w') as f:
        # f.write(r.content)
    d = parse(r.content)
    # print str(d)
    return d


def parse(content):
    a = json.loads(content)
    # print a.keys()
    comments = a["comments"]
    ds = []
    for c in comments:
        d = {}
        d["content"] = c["content"]
        d["pubtime"] = get_real_time(c["create_time"])
        ds.append(d)
    return ds

def get_real_time(s):
    try:
        t = re.search(u'(.*?)([分:小:天:月:年])', s).groups()
    except:
        print 'get_real_time in mg error.'
        exit()
    c = int(t[0])
    if t[1] == u'分':
        tt = time.time() - c*60
    if t[1] == u'小':
        tt = time.time() - c*3600
    if t[1] == u'天':
        tt = time.time() - c*24*3600
    if t[1] == u'月':
        tt = time.time() - c*30*24*3600
    if t[1] == u'年':
        tt = time.time() - c*12*30*24*3600
    tt = int(tt)/10000*10000
    # print tt
    return tt

if __name__ == '__main__':
    vrl = 'http://www.mgtv.com/b/303260/3722682.html'
    vrl = 'http://www.hunantv.com/v/1/5284/f/404653.html'
    vrl = 'http://www.hunantv.com/v/2/49769/f/616205.html'
    main(vrl)
