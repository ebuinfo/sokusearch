#!/usr/bin/env python
# coding: utf-8

import re
import time
import json
import urllib
import requests
import traceback

from push_db import push_comment

def nene(aid, commentid=0):
    base = 'https://coral.qq.com/article/%s/comment?' % str(aid)
    q = [
        ('commentid', str(commentid)),
        ('reqnum', "50"),
        ('tag', ""),
    ]
    query = urllib.urlencode(q)
    return (base + query)

Gsession = requests.Session()
Gsession.headers.update({
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
    })

def get_artid_url(vid):
    base = 'https://ncgi.video.qq.com/fcgi-bin/video_comment_id?'
    q = [
        ('otype', 'json'),
        ('op', '3'),
        ('vid', str(vid)),
    ]
    query = urllib.urlencode(q)
    url = base + query
    return url

def get_comment(source_url):
    html = ''
    vid = 0
    i = 2
    s = requests.Session()
    s.headers.update({
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
    })
    while i >= 1:
        try:
            r = s.get(source_url, verify=False)
            if r.status_code == 200:
                html = r.content
                break
        except:
            i -= 1
            time.sleep(4)
    if html != '':
        re_vid = re.compile(r'.*\s+vid: "([a-z0-9A-Z]+)"', re.S)
        m = re.match(re_vid, html)
        if m:
            vid = m.groups()[0]
        if vid:
            art_url = get_artid_url(vid)
            artid = 0
            try:
                r = s.get(art_url, verify=False)
                if r.status_code == 200:
                    re_artid = re.compile(r'.*comment_id.*"(\d+)"')
                    m = re.match(re_artid, r.content)
                    if m:
                        artid = m.groups()[0]
            except:
                return
            if artid:
                commentid = 0
                while 1:
                    comm_url = nene(artid, commentid)
                    ll = parse_comment(comm_url)
                    l = ll[0]
                    commentid = ll[1]
                    if commentid is None:
                        return
                    s = push_comment(l, source_url)
                    time.sleep(6)

                    if  s < 1:
                        return

def parse_comment(comm_url):
    global Gsession
    l = []
    commentid = None
    try:
        print comm_url
        r = Gsession.get(comm_url, verify=False)
        print 'response code:', r.status_code
        if r.status_code == 200:
            print '[ OK ]'
            #with open('dada.json', 'w') as f:
            #    f.write(r.content)
            d = r.json()
            commentid = d['data']['last']
            print 'last comment id:', commentid
            data = d['data']['commentid']
            for i in data:
                dd = {}
                dd['content'] = i['content'].encode('utf8')
                dd['pubtime'] = i['time']
                # print '.',
                l.append(dd)
    except:
        traceback.print_exc()
        pass
    finally:
        return (l, commentid)

if __name__ == '__main__':
    # comments = parse_comment(nene(1620207797, "6206112417074199077"))
    # for c in comments[0]:
    #     for k in c:
    #         print k, ":", c[k]
    url = ''
    get_comment(url)
