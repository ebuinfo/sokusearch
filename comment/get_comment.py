#!/usr/bin/python
# coding=utf-8


from gevent import monkey; monkey.patch_all()
import gevent
import sys
sys.path.append('../')
import fnvhash
import time
from comment_config import db
import datetime
import comment_youku
import comment_tencent
import comment_letv
import comment_sohu
import comment_tudou
import comment_iqiyi
import comment_mg


class Getcomment(object):
    def __init__(self):
        self.mang_dir = {}
        self.tudou_tv_list = []
        self.youku_tv_list = []
        self.mangguo_tv_list = []
        self.leshi_tv_list = []
        self.qq_tv_list = []
        self.sohu_tv_list = []
        self.mangguo_tv_info_dir = {}
        self.youku_tv_info_dir = {}
        self.tudou_tv_info_dir = {}
        self.kankan_tv_list = []
        self.pps_tv_list = []
        self.iqiyi_tv_list = []
        self.wasu_tv_list = []
        self.mix_list = []
        self.do_db = db

        # set the monitor time limit in 90 days
        pre_date = (datetime.date.today() - datetime.timedelta(days = 30)).strftime("%Y-%m-%d %H:%M:%S")
        print pre_date
        # sql = 'SELECT url FROM video where kid=259 order by id desc'
        sql = 'SELECT url FROM video where create_time > \'%s\' order by id desc' % pre_date
        print sql
        self.url_dir_list = self.do_db.query(sql)
        print "len(url)", len(self.url_dir_list)
        time.sleep(2)
        # for url_dir in self.url_dir_list:
        #     self.mix_list.append(url_dir['url'])
        self.mix_list = [i['url'] for i in self.url_dir_list]

    def pick_up_tv_list_from_mix_list(self):
        self.tudou_tv_list = []
        self.youku_tv_list = []
        self.mangguo_tv_list = []
        self.sohu_tv_list = []
        self.iqiyi_tv_list = []
        self.kankan_tv_list = []
        self.qq_tv_list = []
        self.leshi_tv_list = []
        for http in self.mix_list:
            if http.startswith('//'):
                http = 'http:' + http
            if 'www.tudou.com' in http:
                self.tudou_tv_list.append(http)
            if 'v.youku.com' in http:
                self.youku_tv_list.append(http)
            if 'www.hunantv.com' in http:
                self.mangguo_tv_list.append(http)
            if 'tv.sohu.com' in http:
                self.sohu_tv_list.append(http)
            if 'le.com' in http:
                self.leshi_tv_list.append(http)
            if 'v.qq.com' in http:
                self.qq_tv_list.append(http)
            if 'vod.kankan.com' in http:
                self.kankan_tv_list.append(http)
            if 'pps.tv' in http:
                self.pps_tv_list.append(http)
            if 'iqiyi.com' in http:
                self.iqiyi_tv_list.append(http)
            if 'wasu.cn' in http:
                self.wasu_tv_list.append(http)
        if len(self.iqiyi_tv_list):
            print "iqiyi:"
            print len(self.iqiyi_tv_list)
        if len(self.tudou_tv_list):
            print "tudou:"
            print len(self.tudou_tv_list)
        if len(self.youku_tv_list):
            print "youku:"
            print len(self.youku_tv_list)
        if len(self.mangguo_tv_list):
            print "mangguo:"
            print len(self.mangguo_tv_list)
        if len(self.sohu_tv_list):
            print "sohu:"
            print len(self.sohu_tv_list)
        if len(self.leshi_tv_list):
            print "letv:"
            print len(self.leshi_tv_list)
        if len(self.qq_tv_list):
            print "qq:"
            print len(self.qq_tv_list)
        if len(self.wasu_tv_list):
            print "wasu:"
            print len(self.wasu_tv_list)

    def _get_kid(self, url):
        sql = "SELECT kid FROM video WHERE url='%s'" % url
        # print sql
        kid = self.do_db.query(sql)
        # print "kid", kid
        kid = kid[0]['kid']
        return kid

    def _get_site_url(self, url):
        site_url = ['v.youku.com',
                'tudou.com',
                'sohu.com',
                'iqiyi.com',
                'hunantv.com',
                'sohu.com',
                'le.com',
                ]
        for u in site_url:
            if u in url:
                return u

    def _get_site_name_hash(self, url):
        site_url = self._get_site_url(url)
        if site_url == 'v.youku.com': site_name = '优酷'
        if site_url == 'tudou.com': site_name = '土豆'
        if site_url == 'iqiyi.com': site_name = '爱奇艺'
        if site_url == 'hunantv.com': site_name = '芒果'
        if site_url == 'sohu.com': site_name = '搜狐'
        if site_url == 'u.com': site_name = '搜狐'
        if site_url == 'le.com': site_name = '乐视'
        else:
            raise Exception('should add site and site name')
        return fnvhash.fnv_32a_str(site_name)

    def crawl_youku(self):
        for u in self.youku_tv_list:
            try:
                comment_youku.get_comment(u)
            except Exception, e:
                print e

    def crawl_tudou(self):
        for u in self.tudou_tv_list:
            try:
                info = comment_tudou.get_comment(u)
                print "push tudou"
            except Exception, e:
                print e

    def crawl_iqiyi(self):
        for u in self.iqiyi_tv_list:
            try:
                info = comment_iqiyi.get_comment(u)
                print "push iqiyi"
            except Exception, e:
                print e

    def crawl_mangguo(self):
        from dive_into_mangguo import dive_into_mangguo
        for u in self.mangguo_tv_list:
            try:
                print u
                info = comment_mg.get_comment(u)
                print "push mangguo"
            except Exception, e:
                print e

    def crawl_sohu(self):
        for u in self.sohu_tv_list:
            try:
                comment_sohu.get_comment(u)
            except Exception, e:
                print e

    def crawl_tencent(self):
        for u in self.qq_tv_list:
            try:
                comment_tencent.get_comment(u)
            except Exception, e:
                print e

    def crawl_le(self):
        for u in self.qq_tv_list:
            try:
                comment_letv.get_comment(u)
            except Exception, e:
                print e

    def start(self):
        self.pick_up_tv_list_from_mix_list()

        gevent.joinall([
            gevent.spawn(self.crawl_youku),
            gevent.spawn(self.crawl_tudou),
            gevent.spawn(self.crawl_sohu),
            gevent.spawn(self.crawl_mangguo),
            gevent.spawn(self.crawl_iqiyi),
            gevent.spawn(self.crawl_tencent),
            # gevent.spawn(self.crawl_le),
            ])


def debug(tv_site):
    gt = Getcomment()
    gt.pick_up_tv_list_from_mix_list()

    if tv_site == 'qq':
        gevent.joinall([ gevent.spawn(gt.crawl_tencent), ])

    if tv_site == 'iqiyi':
        gevent.joinall([ gevent.spawn(gt.crawl_iqiyi), ])

    if tv_site == 'mangguo':
        gevent.joinall([ gevent.spawn(gt.crawl_mangguo), ])

    if tv_site == 'youku':
        # gt.youku_tv_list = ['http://v.youku.com/v_show/id_XMTM3Mjk0ODMyNA==.html']
        # gt.youku_tv_list = ['http://v.youku.com/v_show/id_XMTM2NTg0MzE4MA==.html']
        gevent.joinall([ gevent.spawn(gt.crawl_youku), ])

    if tv_site == 'tudou':
        gevent.joinall([ gevent.spawn(gt.crawl_tudou), ])

    if tv_site == 'sohu':
        gevent.joinall([ gevent.spawn(gt.crawl_sohu), ])

    if tv_site == 'letv':
        gevent.joinall([ gevent.spawn(gt.crawl_le), ])

    print 'over'


def start():
    gt = Getcomment()
    gt.start()


def mession_loop():
    while(1):
        print "ko"
        start()
        # time.sleep(3600 * 24)
        time.sleep(3)


if __name__ == '__main__':
    from sys import argv

    if len(argv) == 1:
        mession_loop()
    else:
        debug(argv[1])
