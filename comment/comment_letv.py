#!/usr/bin/env python
# coding: utf-8

import re
import time
import json
import urllib
import requests

from push_db import push_comment

def nene(cid, pid, vid, page=1):
    base = 'http://api.my.le.com/vcm/api/list?'
    q = [
        ('cid', str(cid)),
        ('pid', str(pid)),
        ('xid', str(vid)),
        ('rows', "50"),
        ('type', "video"),
        ('sort', ""),
        ('ctype', "cmt,img,vote"),
        ('listTyple', "1"),
        ('source', "1"),
        ('page', str(page)),
    ]
    query = urllib.urlencode(q)
    return base + query

Gsession = requests.Session()
Gsession.headers.update({
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
    })

def get_comment(source_url):
    html = ''
    cid = 0
    pid = 0
    vid = 0
    i = 2
    while i >= 1:
        try:
            r = requests.get(source_url, headers={
'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
                })
            if r.status_code == 200:
                html = r.content
                break
        except:
            i -= 1
            time.sleep(4)
    if html != '':
        re_cid = re.compile(r'.*\s+cid: (\d+)', re.S)
        re_pid = re.compile(r'.*\s+pid: (\d+)', re.S)
        re_vid = re.compile(r'.*\s+vid: (\d+)', re.S)
        m = re.match(re_cid, html)
        if m:
            cid = m.groups()[0]
        m = re.match(re_pid, html)
        if m:
            pid = m.groups()[0]
        m = re.match(re_vid, html)
        if m:
            vid = m.groups()[0]
        if cid and pid and vid:
            for x in range(1, 500):
                ll = parse_comment(nene(cid, pid, vid, page=x))
                s = push_comment(ll, source_url)
                time.sleep(6)

                if  s < 1:
                    return

def parse_comment(comm_url):
    global Gsession
    l = []
    try:
        r = Gsession.get(comm_url)
        if r.status_code == 200:
            d = json.loads(r.text)
            data = d['data']
            for i in data:
                dd = {}
                dd['content'] = i['content'].encode('utf8')
                dd['pubtime'] = i['ctime']
                print '.',
                l.append(dd)
    except:
        pass
    finally:
        return l

def main(url):
    get_comment(url)


if __name__ == '__main__':
    # comments = parse_comment(nene(2, 73868, 1578861, page=2))
    # for c in comments:
    #     for k in c:
    #         print k, ":", c[k]
    main(url)
