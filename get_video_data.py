# coding=utf-8

from selenium import webdriver
import urllib
import re
from toolkitv import MySQLUtility
import fnvhash
import time
import threading
import requests
from db_info import db
OPEN = 1
CLOSE = 0

SOUKU_URL = 'http://www.soku.com/'
TEST = 0


class GetInfoFromSoku(object):

    def __init__(self):
        self.mang_dir = {}
        self.toudou_tv_list = []
        self.youku_tv_list = []
        self.mangguo_tv_list = []
        self.leshi_tv_list = []
        self.qq_tv_list = []
        self.souhu_tv_list = []
        self.mangguo_tv_info_dir = {}
        self.youku_tv_info_dir = {}
        self.tudou_tv_info_dir = {}
        self.kankan_tv_list = []
        self.pps_tv_list = []
        self.iqiyi_tv_list = []
        self.wasu_tv_list = []
        self.mix_list = []
        self.i = 0
        self.OUTLOG = CLOSE
        self.tv_num_dir = {}
        self.log_list = []
        self.undown_list = []
        self.do_db = db['self']

        # sql = 'SELECT url FROM video WHERE create_time 1'
        sql = 'SELECT url FROM video where DATE_SUB(CURDATE(), INTERVAL 90 DAY) <= date(create_time )'
        self.url_dir_list = self.do_db.query(sql)
        for url_dir in self.url_dir_list:
            self.mix_list.append(url_dir['url'])

    def pick_up_tv_list_from_mix_list(self, mix_list):
        self.toudou_tv_list = []
        self.youku_tv_list = []
        self.mangguo_tv_list = []
        self.souhu_tv_list = []
        self.iqiyi_tv_list = []
        self.kankan_tv_list = []
        self.qq_tv_list = []
        self.leshi_tv_list = []
        for http in mix_list:
            if 'www.tudou.com' in http:
                self.toudou_tv_list.append(http)
            if 'v.youku.com' in http:
                self.youku_tv_list.append(http)
            if 'www.hunantv.com' in http:
                self.mangguo_tv_list.append(http)
            if 'tv.sohu.com' in http:
                self.souhu_tv_list.append(http)
            if 'www.letv.com' in http:
                self.leshi_tv_list.append(http)
            if 'v.qq.com' in http:
                self.qq_tv_list.append(http)
            if 'vod.kankan.com' in http:
                self.kankan_tv_list.append(http)
            if 'pps.tv' in http:
                self.pps_tv_list.append(http)
            if 'iqiyi.com' in http:
                self.iqiyi_tv_list.append(http)
            if 'wasu.cn' in http:
                self.wasu_tv_list.append(http)
        if len(self.iqiyi_tv_list):
            print "iqiyi:"
            print len(self.iqiyi_tv_list)
        if len(self.toudou_tv_list):
            print "tudou:"
            print len(self.toudou_tv_list)
        if len(self.youku_tv_list):
            print "youku:"
            print len(self.youku_tv_list)
        if len(self.mangguo_tv_list):
            print "mangguo:"
            print len(self.mangguo_tv_list)
        if len(self.souhu_tv_list):
            print "sohu:"
            print len(self.souhu_tv_list)
        if len(self.leshi_tv_list):
            print "letv:"
            print len(self.leshi_tv_list)
        if len(self.qq_tv_list):
            print "qq:"
            print len(self.qq_tv_list)
        if len(self.wasu_tv_list):
            print "wasu:"
            print len(self.wasu_tv_list)

    def tool_38d02y_to_380200(self, c):
        try:
            wan_yi = re.search(r'[^\d]', c.split('.')[1]).group()
        except Exception, e:
            wan_yi = re.search(r'[^\d]', c.split('.')[0]).group()
        b = float(c.split(wan_yi)[0])
        c_dir = {u'万': 10000, u'亿': 100000000, ',': 1}
        play = b * c_dir[wan_yi]
        return play

    def format_int(self, temp):
        temp = temp.replace(',', '')
        if u'万' in temp:
            temp = temp.replace(u'万', '')
            temp = int(temp) * 10000
        return int(temp)

    def get_kid_creat(self, url, do_db):
        sql = "SELECT kid FROM video WHERE url='%s'" % url
        # print sql
        kid = do_db.query(sql)
        # print "kid", kid
        kid = kid[0]['kid']
        sql = "SELECT create_at FROM crawl_words WHERE id=%d" % kid
        create_at = do_db.query(sql)
        # print "create_at", create_at
        if len(create_at) == 0:
            return -1, -1
        create_at = create_at[0]['create_at']
        return kid, create_at

    def get_real_num(self, a):
        if type(a) == int:
            return a
        b = a.encode("utf-8").split(',')
        if len(b) == 4:
            d = int(b[0]) * 1000000000 + int(b[1]) * 1000000 + int(b[2]) * 1000 + int(b[3])
        if len(b) == 3:
            d = int(b[0]) * 1000000 + int(b[1]) * 1000 + int(b[2])
        if len(b) == 2:
            d = int(b[0]) * 1000 + int(b[1])
        if len(b) == 1:
            d = int(b[0])
        return d

    def make_info_dir_video_and_data(self, url_hash, site_hash, site_url, play, good, bad, comment, kid, create_at):
        import datetime
        info_dir_vedio_data = {}

        info_dir_vedio_data['url_hash'] = url_hash
        info_dir_vedio_data['views'] = play
        info_dir_vedio_data['good'] = good
        info_dir_vedio_data['bad'] = bad
        info_dir_vedio_data['kid'] = kid
        info_dir_vedio_data['site_hash'] = site_hash
        info_dir_vedio_data['comment_num'] = comment
        info_dir_vedio_data['create_at'] = str(datetime.date.today() - datetime.timedelta(days=1))

        info_dir = {
            'info_dir_vedio_data': info_dir_vedio_data}
        return info_dir


    def dive_into(self, site, site_tv_list, s):
        service_args = ['--load-images=false']
        self_driver = webdriver.PhantomJS(service_args=service_args)
        self_driver.set_page_load_timeout(s)

        sum = len(site_tv_list)
        i = 0
        for url in site_tv_list:
            i += 1
            print "[*] %s remain %d" % (site, sum-i)
            try:
                if site == 'youku':
                    self.dive_into_youku(url, self_driver)
            except Exception:
                print "[*] %s PASS %s" % (site, url)
                open("outlog_data_pass.log", 'a').write("%s\tpass\t%s\n" % i(site, url))
        print "[*] %s down" % site
        self_driver.close()

    def dive_into_iqiyi(self, url, kid, create_at, do_db):
        r = requests.get(url, timeout=20)
        # if r.status_code == 404:
        #     return

        # get vid
        id_html = r.text
        reg = 'tvId\:(.*?)\,'
        vid = int(re.search(reg, id_html).groups()[0])

        pre_url = 'http://mixer.video.iqiyi.com/jp/recommend/videos?referenceId=%s&withRefer=true&type=video'
        pre_url %= str(vid)
        r = requests.get(pre_url, timeout=20)
        id_html = r.text
        # get play up down comm
        reg = r'%s[\w\W]*?%s[\w\W]*?%s' % (vid, vid, vid)
        temp = re.search(reg, id_html).group()
        reg = r'upCount\"\:(.*?)\,'
        up = re.search(reg, temp).groups()[0]
        reg = r'downCount\"\:(.*?)\,'
        down = re.search(reg, temp).groups()[0]
        reg = r'playCount"\:(.*?)\,'
        play = re.search(reg, temp).groups()[0]
        reg = r'commentCount"\:(.*?)\,'
        comment = re.search(reg, temp).groups()[0]
        print '\t', up, '\t', down, '\t', play, '\t', comment, '\t', url

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'iqiyi.com'
        site_hash = fnvhash.fnv_32a_str('奇艺')

        info_dir = self.make_info_dir_video_and_data(url_hash, site_hash, site_url, play, up, down, comment, kid, create_at)
        do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])

    def dive_into_leshi(self, url, self_driver):
        html = requests.get(url, timeout=20).text
        reg = r'vid\:(\d*?)\,'
        vid = int(re.search(reg, html).groups()[0])

        url_re = 'http://stat.letv.com/vplay/queryMmsTotalPCount?vid=%s' % vid
        html = requests.get(url_re, timeout=20).text
        reg = r'media\_play\_count\"\:(.*?)\,'
        play = int(re.search(reg, html).groups()[0])
        reg = r'omm\_count\"\:(.*?)\,'
        comment = int(re.search(reg, html).groups()[0])

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'letv.com'
        site_hash = fnvhash.fnv_32a_str('乐视')
        good = 0
        bad = 0

        do_db = db['iqiyi']
        kid, create_at = self.get_kid_creat(url, do_db)
        info_dir = self.make_info_dir_video_and_data(url_hash, site_hash, site_url, play, good, bad, comment, kid, create_at)
        do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])
        # print "---------------乐视-----------------"
        # print "title:",title
        # print "date:", date
        # print "play:", play
        # print "comment:",comment
        # print "url:", url

    def dive_into_qq(self, url, self_driver):
        try:
            self_driver.get(url)
            if self.OUTLOG == OPEN:
                time.sleep(4)
            else:
                time.sleep(2)
        except Exception:
            pass
        action_temp = self_driver.find_element_by_class_name("action_count").text
        comment = action_temp.split('\n')[0].split(u'条评论')[0]
        play_temp = action_temp.split('\n')[1].split(u'次播放')[0]
        #c_dir = {u'万': 10000, u'亿': 100000000, ',': 1}
        a = float(play_temp.split()[0])
        play = a * 10000# + c_dir[play_temp.split()[1]]

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'v.qq.com'
        site_hash = fnvhash.fnv_32a_str('腾讯')
        good = 0
        bad = 0
        do_db = db['qq']
        kid, create_at = self.get_kid_creat(url, do_db)
        info_dir = self.make_info_dir_video_and_data(url_hash, site_hash, site_url, play, good, bad, comment, kid, create_at)
        do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])

    def dive_into_tudou(self, url, kid, create_at, do_db):
        if 'v.tv.sohu' in url:
            return
        r = requests.get(url, timeout=20)
        if r.status_code == 404:
            return

        rreg = r',iid: .*[^\d]'
        iid = re.search(rreg, r.text).group()
        payload = {'jsoncallback':'page_play_model_itemSumModel__find', 'app':'4', 'showArea':'true', 'iabcdefg':iid.split()[1].encode('utf-8'), 'uabcdefg':'0', 'juabcdefg':'019dbmdt972c4n'}
        r1 = requests.get('http://www.tudou.com/crp/itemSum.action', params = payload)
        a = r1.text.split('{')[1].split('}')[0]
        b = a.split(',')
        nn = []
        for mm in b:
            nn.append(int(mm.split(':')[1]))

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'tudou.com'
        site_hash = fnvhash.fnv_32a_str('土豆')
        comment = nn[1]
        play = nn[8]
        bad = 0
        good = nn[4]

        print '\t', nn[4], '\t', play, '\t', comment, '\t', url
        info_dir = self.make_info_dir_video_and_data(url_hash, site_hash, site_url, play, good, bad, comment, kid, create_at)
        do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])

    def dive_into_sohu(self, url, kid, create_at, do_db):
        if 'v.tv.sohu' in url:
            return
        r = requests.get(url, timeout=20)
        if r.status_code == 404:
            return
        print url

        id_html = r.text
        reg = r'var vid\=\"(.*?)\"'
        id = re.search(reg, id_html).groups()[0]
        reg = r'var playlistId\=\"(\d*?)\"'
        playlistid = re.search(reg, id_html).groups()[0]
        if playlistid == '': playlistid = str(0)
        reg = r'var cid\=\"(.*?)\"'
        typeid = re.search(reg, id_html).groups()[0]
        if typeid == '': typeid = str(0)

        url_pre = 'http://score.my.tv.sohu.com/digg/get.do?vid=%s&type=%s'
        url_pre %= (id, typeid)
        html = requests.get(url_pre, timeout=20).text

        # get up / down
        reg = r'upCount\"\:(\d*?)\,'
        up = re.search(reg, html).groups()[0]
        up = self.get_real_num(up)
        reg = r'downCount\"\:(\d*?)\}'
        down = re.search(reg, html).groups()[0]
        down = self.get_real_num(down)

        url_pre = 'http://count.vrs.sohu.com/count/queryext.action?vids=%s'
        url_pre %= id
        html = requests.get(url_pre, timeout=20).text

        # get play
        reg = 'total\"\:(\d*?)\,'
        play = re.search(reg, html).groups()[0]
        play = self.get_real_num(play)
        # print play

        url_pre = 'http://access.tv.sohu.com/reply/list/%s_%s_%s_0_10.js'
        url_pre %= (typeid, playlistid, id)
        html = requests.get(url_pre, timeout=20).text

        # get comment
        reg = r'allCount\"\:(.*?)\,'
        comment = re.search(reg, html).groups()[0]
        comment = self.get_real_num(comment)

        print '\t', up, '\t', down, '\t', play, '\t', comment, '\t', url

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'tv.sohu.com'
        site_hash = fnvhash.fnv_32a_str('搜狐')
        info_dir = self.make_info_dir_video_and_data(url_hash, site_hash, site_url, play, up, down, comment, kid, create_at)
        do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])

    def dive_into_youku(self, url, kid, create_at, do_db):
        r = requests.get(url, timeout=20)
        if r.status_code == 404:
            return

        id_html = r.text
        reg = r'var videoId \= \'(.*?)\''
        id = re.search(reg, id_html).groups()[0]

        url_pre = 'http://v.youku.com/v_vpactionInfo/id/%s/pm/3?__rt=1&__ro=info_stat'
        sec_url = url_pre % id
        html = requests.get(sec_url, timeout=20).text
        # print html

        # get up/down
        reg = ur'踩.*?\"\>(.*?)\<'
        temp = re.search(reg, html).groups()[0]
        up = self.get_real_num(temp.split('/')[0])
        down = self.get_real_num(temp.split('/')[1])
        # print up, down

        # get comment
        reg = ur'评论.*?\"\>(.*?)\<'
        comment = re.search(reg, html).groups()[0]
        if comment == '':
            comment = 0
        comment = self.get_real_num(comment)

        # get play
        reg = ur'总播放.*?\"\>(.*?)\<'
        play = re.search(reg, html).groups()[0]
        play = self.get_real_num(play)

        print '\t', up, '\t', down, '\t', play, '\t', comment, '\t', url

        # push into db
        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'v.youku.com'
        site_hash = fnvhash.fnv_32a_str('优酷')
        good = up
        bad = down
        info_dir = self.make_info_dir_video_and_data(url_hash, site_hash, site_url, play, good, bad, comment, kid, create_at)
        do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])

    def dive_into_mangguo(self, self_driver, url, kid, create_at, do_db):
        page = urllib.urlopen(url)
        page_data = page.read()
        reg = r'release_date\: ".*?\"'
        #reg = r'release_date.*?\"\,'
        #reg = r'window.VI[\w\W]+?\}'
        imgre = re.compile(reg)
        imglist = re.search(imgre, page_data).group()
        m = imglist.split('"')
        try:
            self_driver.get(url)
            time.sleep(2)
        except Exception:
            pass
        comment = self_driver.find_element_by_class_name('comments-total-nums').text
        up = self_driver.find_element_by_class_name('eln-1').text
        down = self_driver.find_element_by_class_name('eln-2').text
        play_temp = self_driver.find_element_by_class_name('play-numall').text.split()[0]

        c_dir = {u'万':10000, u'亿':100000000, ',':1}
        resu = re.search(r'[0-9]*', play_temp)
        a = int(resu.group())
        d = a
        su = re.search(r',', play_temp)
        if su:
            m = play_temp.split(',')
            d = int(m[0]) * 1000 + int(m[1])
        else:
            b = re.search(r'[^\d]', play_temp).group()
            if b == u'.':
                bb = play_temp.split('.')[1]
                b = re.search(r'[^\d]', bb).group()
                a = play_temp.split(b)[0]
            d = float(a) * c_dir[b]
        play = d

        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'hunantv.com'
        site_hash = fnvhash.fnv_32a_str('芒果')
        good = up
        bad = down

        print '\t', up, '\t', down, '\t', play, '\t', comment, '\t', url
        info_dir = self.make_info_dir_video_and_data(url_hash, site_hash, site_url, play, good, bad, comment, kid, create_at)
        do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])

    def dive_into_wasu(self, self_driver, url, kid, create_at, do_db):
        try:
            self_driver.get(url)
            time.sleep(6)
        except Exception:
            pass
        # get play
        self_driver.get(url)
        html = self_driver.page_source
        reg = r'\"play\_vod\_hits\"\>(.*?)\<\/spa'
        play = re.search(reg, html).groups()[0]
        play = self.get_real_num(play)
        print '\t', play, '\t', url

        # push into db
        url_hash = fnvhash.fnv_64a_str(url)
        site_url = 'wasu.cn'
        site_hash = fnvhash.fnv_32a_str('华数')
        good = 0
        bad = 0
        comment = 0
        info_dir = self.make_info_dir_video_and_data(url_hash, site_hash, site_url, play, good, bad, comment, kid, create_at)
        do_db.item_to_table('video_data', info_dir['info_dir_vedio_data'])

    def get_tv_info_from_youku(self):

        sum = len(self.youku_tv_list)
        i = -1
        for url in self.youku_tv_list:
            i += 1
            print "yoku", sum-i
            try:
                do_db = db['youku']
                kid, create_at = self.get_kid_creat(url, do_db)
                if create_at is not -1:
                    self.dive_into_youku(url, kid, create_at, do_db)
                    time.sleep(2)
            except Exception, e:
                print "[*] YOUKU PASS", url
                open("lla_data_log.log", 'a').write("%s\n" % url)
                open("lla_data_log.log", 'a').write("remain %d\n" % (sum-i))
                open("lla_data_log.log", 'a').write("e:%s\n" % e)
                self.log_list.append(url)
        print "[*] youku down"
        open("outlog_data_dwon.log", 'a').write("youku down\n")

    def get_tv_info_from_sohu(self):

        sum = len(self.souhu_tv_list)
        i = -1
        for url in self.souhu_tv_list:
            i += 1
            print "sohu", sum-i
            try:
                do_db = db['sohu']
                kid, create_at = self.get_kid_creat(url, do_db)
                if create_at is not -1:
                    self.dive_into_sohu(url, kid, create_at, do_db)
                    time.sleep(2)
            except Exception, e:
                print "[*]SOHU PASS ", url, e
                open("lla_data_log.log", 'a').write("sohu\tpass\t%s\n" % url)
                open("lla_data_log.log", 'a').write("remain %d\n" % (sum-i))
                self.log_list.append(url)
                open("lla_data_log.log", 'a').write("e:%s\n" % e)
        print "[*] sohu down"
        open("outlog_data_dwon.log", 'a').write("sohu down\n")

    def with_phantomjs(func):
         import functools
         @functools.wraps(func)
         def wrapper(*argvs, **kwargvs):
             service_args = ['--load-images=false']
             self_driver = webdriver.PhantomJS(service_args=service_args)
             self_driver.set_page_load_timeout(8)
             return func(*argvs, **kwargvs)
         return wrapper

    @with_phantomjs
    def get_tv_info_from_wasu(self):
        sum = len(self.wasu_tv_list)
        i = -1
        for url in self.wasu_tv_list:
            i += 1
            print "wasu", sum-i
            try:
                do_db = db['wasu']
                kid, create_at = self.get_kid_creat(url, do_db)
                if create_at is not -1:
                    self.dive_into_wasu(self_driver, url, kid, create_at, do_db)
                    time.sleep(2)
            except Exception, e:
                print "[*] wasu PASS ", url
                open("lla_data_log.log", 'a').write("wasu\tpass\t%s\n" % url)
                open("lla_data_log.log", 'a').write("remain %d\n" % (sum-i))
                open("lla_data_log.log", 'a').write("e:%s\n" % e)
                self.log_list.append(url)
        self_driver.close()
        print "[*] wasu down"
        open("lla_data_log.log", 'a').write("[*] wasu down")

    @with_phantomjs
    def get_tv_info_from_mangguo(self):
        sum = len(self.mangguo_tv_list)
        i = -1
        for url in self.mangguo_tv_list:
            i += 1
            print "mangguo", sum-i
            try:
                do_db = db['mangguo']
                kid, create_at = self.get_kid_creat(url, do_db)
                if create_at is not -1:
                    self.dive_into_mangguo(self_driver, url, kid, create_at, do_db)
                    time.sleep(2)
            except Exception, e:
                print "[*] MANGGUO PASS ", url
                open("lla_data_log.log", 'a').write("mangguo\tpass\t%s\n" % url)
                open("lla_data_log.log", 'a').write("remain %d\n" % (sum-i))
                open("lla_data_log.log", 'a').write("e:%s\n" % e)
                self.log_list.append(url)
        self_driver.close()
        print "[*] mangguo down"
        open("lla_data_log.log", 'a').write("[*] mangguo down")

    @with_phantomjs
    def get_tv_info_from_leshi(self):
        sum = len(self.leshi_tv_list)
        i = -1
        for url in self.leshi_tv_list:
            i += 1
            print "letv", sum-i
            try:
                self.dive_into_leshi(url, self_driver)
            except Exception, e:
                print "[*] leshi PASS ", url
                open("lla_data_log.log", 'a').write("leshi\tpass\t%s\n" % url)
                open("lla_data_log.log", 'a').write("remain %d\n" % (sum-i))
                open("lla_data_log.log", 'a').write("e:%s\n" % e)
                self.log_list.append(url)
            # self.dive_into_leshi(url, self_driver)
        self_driver.close()
        print "[*] leshi down"
        open("lla_data_log.log", 'a').write("[*] leshi down")

    @with_phantomjs
    def get_tv_info_from_iqiyi(self):
        sum = len(self.iqiyi_tv_list)
        i = -1
        for url in self.iqiyi_tv_list:
            i += 1
            print "iqiyi", sum-i
            try:
                do_db = db['iqiyi']
                kid, create_at = self.get_kid_creat(url, do_db)
                if create_at is not -1:
                    self.dive_into_iqiyi(url, kid, create_at, do_db)
                    time.sleep(2)
            except Exception, e:
                print "iqiyi PASS ", url
                open("lla_data_log.log", 'a').write("iqiyi\tpass\t%s\n" % url)
                open("lla_data_log.log", 'a').write("remain %d\n" % (sum-i))
                open("lla_data_log.log", 'a').write("e:%s\n" % e)
                self.log_list.append(url)
        self_driver.close()
        print "[*] iqiyi down"
        open("lla_data_log.log", 'a').write("[*] iqiyi down")

    def get_tv_info_from_tudou(self):
        sum = len(self.toudou_tv_list)
        i = -1
        for url in self.toudou_tv_list:
            i += 1
            print "tudou", sum-i
            do_db = db['tudou']
            try:
                kid, create_at = self.get_kid_creat(url, do_db)
                print "kc",kid ,create_at
                if create_at is not -1:
                    self.dive_into_tudou(url, kid, create_at, do_db)
                    time.sleep(2)
            except Exception, e:
                print "tudou PASS ", url
                open("lla_data_log.log", 'a').write("tudou\tpass\t%s\n" % url)
                open("lla_data_log.log", 'a').write("remain %d\n" % (sum-i))
                open("lla_data_log.log", 'a').write("e:%s\n" % e)
                self.log_list.append(url)
        print "[*] tudou down"
        open("lla_data_log.log", 'a').write("[*] tudou down")

    @with_phantomjs
    def get_tv_info_from_qq(self):
        sum = len(self.qq_tv_list)
        i = -1
        for url in self.qq_tv_list:
            try:
                i += 1
            except Exception, e:
                print "[*]qq PASS ", url
                open("lla_data_log.log", 'a').write("qq\tpass\t%s\n" % url)
                open("lla_data_log.log", 'a').write("remain %d\n" % (sum-i))
                open("lla_data_log.log", 'a').write("e:%s\n" % e)
                self.log_list.append(url)
        self_driver.close()
        print "[*] qq down"
        open("lla_data_log.log", 'a').write("[*] qq down")

    def get_tv_info_from_tv_list(self):
        threads = []
        t1 = threading.Thread(target=self.get_tv_info_from_youku)
        t2 = threading.Thread(target=self.get_tv_info_from_tudou)
        t3 = threading.Thread(target=self.get_tv_info_from_mangguo)
        t5 = threading.Thread(target=self.get_tv_info_from_leshi)
        t6 = threading.Thread(target=self.get_tv_info_from_iqiyi)
        t7 = threading.Thread(target=self.get_tv_info_from_qq)
        t8 = threading.Thread(target=self.get_tv_info_from_sohu)
        t9 = threading.Thread(target=self.get_tv_info_from_wasu)

        if self.youku_tv_list:
            print "[*]add youku"
            open("lla_data_log.log", 'a').write("add youku\n")
            threads.append(t1)
        if self.souhu_tv_list:
            open("lla_data_log.log", 'a').write("add sohu\n")
            print "[*]add sohu"
            threads.append(t8)
        if self.toudou_tv_list:
            open("lla_data_log.log", 'a').write("add tudou\n")
            print "[*]add tudou"
            threads.append(t2)
        if self.mangguo_tv_list:
            print "[*]add mangguo"
            open("lla_data_log.log", 'a').write("add mangguo\n")
            threads.append(t3)
        # if self.kankan_tv_list:
        #     print "[*]add kankan"
        #     open("lla_data_log.log", 'a').write("add kankan\n")
        #     threads.append(t4)
        if self.leshi_tv_list:
            print "[*]add leshi"
            open("lla_data_log.log", 'a').write("add leshi\n")
            threads.append(t5)
        if self.iqiyi_tv_list:
            print "[*]add iqiyi"
            open("lla_data_log.log", 'a').write("add iqiyi\n")
            threads.append(t6)
        if self.qq_tv_list:
            print "[*]add qq"
            open("lla_data_log.log", 'a').write("add qq\n")
            threads.append(t7)
        if self.wasu_tv_list:
            print "[*]add wasu"
            open("lla_data_log.log", 'a').write("add wasu\n")
            threads.append(t9)

        print "threads:", threads
        print "sleep 10"
        time.sleep(10)
        i = threads[0]
        for j in threads:
            i = j
            i.setDaemon(True)
            i.start()
            print "[***] %d" % threading.activeCount()
        for t in threads:
            t.join()

        open("lla_data_log.log", 'a').write("time: %s\n" % time.ctime())
        open("lla_data_log.log", 'a').write("********************************\n")


def start():
    from sys import exit
    info = GetInfoFromSoku()
    info.pick_up_tv_list_from_mix_list(info.mix_list)
    if not info.mix_list:
        print "url or tv name none"
        info.driver.quit()
        info.driver.quit()
        exit()

    open("lla_data_log.log", 'a').write("*******************start******************\n")
    open("lla_data_log.log", 'a').write("***********%s*********\n" % time.ctime())
    info.get_tv_info_from_tv_list()

    # reget from outlog_url
    if info.log_list:
        print "log_list:", len(info.log_list)
    if info.undown_list:
        print "undown_list:", len(info.undown_list)

    info.OUTLOG = OPEN
    info.mix_list = info.log_list
    if not info.log_list:
        open("lla_data_log.log", 'a').write("work well down!\n" )
        open("lla_data_log.log", 'a').write("*****%s****************\n" % time.ctime())
    else:
        open("lla_data_log.log", 'a').write("************except*************\n")
        for ii in info.mix_list:
            open("lla_data_log.log", 'a').write("%s\n" % ii )
        info.pick_up_tv_list_from_mix_list(info.mix_list)
        if info.undown_list:
            open("lla_data_log.log", 'a').write("************undown*************\n")
            for ii in info.undown_list:
                open("lla_data_log.log", 'a').write("%s\n" % ii )
        else:
            open("lla_data_log.log", 'a').write("[*]undown repaired\n")

    open("lla_data_log.log", 'a').write("all down")
    open("lla_data_log.log", 'a').write("%s\n" % time.ctime())
    open("lla_data_log.log", 'a').write("*******************************\n")

    print "[*] all down"
    exit()


if __name__ == '__main__':
    from sys import argv
    if len(argv) == 1:
        start()
    else:
        if argv[1] == 'iqiyi':
            print "test"
            info = GetInfoFromSoku()
            info.mix_list = []
            info.mix_list = [
                'http://www.iqiyi.com/v_19rrh5gozs.html'
            ]
            info.pick_up_tv_list_from_mix_list(info.mix_list)
            info.get_tv_info_from_iqiyi()

        if argv[1] == 'wasu':
            print "test"
            info = GetInfoFromSoku()
            info.mix_list = []
            info.mix_list = [
                'http://www.wasu.cn/wap/play/show/id/5341260',
                'http://www.wasu.cn/Play/show/id/2729993',
            ]
            info.pick_up_tv_list_from_mix_list(info.mix_list)
            info.get_tv_info_from_wasu()

        if argv[1] == 'tudou':
            print "test"
            info = GetInfoFromSoku()
            info.mix_list = []
            info.mix_list = [
                'http://www.tudou.com/albumplay/hXTcb4f5Mpc/xnGugOCg9iM.html',
            ]
            info.pick_up_tv_list_from_mix_list(info.mix_list)
            info.get_tv_info_from_tudou()

        if argv[1] == 'sohu':
            print "test"
            info = GetInfoFromSoku()
            info.mix_list = []
            info.mix_list = [
                # 'http://tv.sohu.com/20130928/n387408596.shtml',
                # 'http://tv.sohu.com/20150402/n410682693.shtml',
                'http://tv.sohu.com/20111211/n328637598.shtml',
                'http://tv.sohu.com/20110921/n320081557.shtml'
            ]
            info.pick_up_tv_list_from_mix_list(info.mix_list)
            info.get_tv_info_from_sohu()

        if argv[1] == 'youku':
            print "test"
            info = GetInfoFromSoku()
            # url = 'http://v.youku.com/v_show/id_XMTYwOTk0Nzg0.html'
            # url = 'http://v.youku.com/v_show/id_XNTcwOTAwMTM2.html'
            # url = 'http://v.youku.com/v_show/id_XOTI0OTk5MzI4.html'
            url = 'http://v.youku.com/v_show/id_XNjQ2NjU1OTY0.html'
            info.mix_list = []
            info.mix_list.append(url)
            info.pick_up_tv_list_from_mix_list(info.mix_list)
            info.get_tv_info_from_youku()
