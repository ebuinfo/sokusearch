#!/usr/bin/env python
#coding=utf-8

# from selenium import webdriver
import urllib
import re
from db_info import db
import fnvhash
import time
import random
# import threading
import requests
from translate_video import get_year_via_title
import logging
logging.basicConfig(level=logging.INFO)

from pubtime import pub

do_db = db['do']


def _get_real_url(url):
    if url.startswith("/search/redirect.html"):
        pp = re.findall(r'url=(.*?)\&', url)
        if pp:
            url = pp[0]
        else:
            return ''
    url = url.split('?')[0]
    if url.startswith('//'):
        url = 'http:' + url
    if url.startswith('/'):
        url = ''

    return url


class GetVideoUrl(object):
    def __init__(self, tv_name, id_=None):
        self.mang_dir = {}
        self.tv_type = ''
        self.tv_num_dir = {}
        self.tv_name = tv_name
        self.do_db = db['self']
        if id_ == None:
            self._get_word_id()
        else:
            self.kid = id_

        # self._get_video_table_url()
        self.clickSouku()
        self.get_tv_list_from_second_page()

    def _get_word_id(self):
        sql = "SELECT id FROM event_def WHERE keywords='%s'" % self.tv_name
        self.craw_dir_list = self.do_db.query(sql)
        self.kid = self.craw_dir_list[0]['id']

    def _get_video_table_url(self):
        sql = "SELECT url FROM video"
        video_url_dir = self.do_db.query(sql)
        self.video_url = [i['url'] for i in video_url_dir]

    def clickSouku(self):
        # url = 'http://www.soku.com/v?keyword=%s' % self.tv_name
        url = 'http://www.soku.com/search_video/q_%s' % self.tv_name
        r = requests.get(url, timeout=10)
        time.sleep(2)
        self.second_page_url = r.url
        logging.info('soku_url %s' % self.second_page_url)

    def get_url_from_href_title_dianshiju(self, href):
        m = href.encode('utf-8').split("'")
        m = m[1].split('?')
        #print "m.split:",m
        self.mang_dir[m[0]] = 'true'
        return m[0]
        #print self.mang_dir.keys()
        #self.mix_list = self.mang_dir.keys()

    def _get_site(self, http):
        if 'www.tudou.com' in http:
            return '土豆'
        if 'v.youku.com' in http:
            return '优酷'
        if 'iqiyi.com' in http:
            return '爱奇艺'
        if 'www.hunantv.com' in http:
            return '芒果'
        if 'www.mgtv.com' in http:
            return '芒果'
        if 'tv.sohu.com' in http:
            return '搜狐'
        if 'www.letv.com' in http:
            return '乐视'
        if 'v.qq.com' in http:
            return '腾讯'
        if 'vod.kankan.com' in http:
            return '迅雷'
        if 'wasu.cn' in http:
            return '华数'
        if 'pptv.com' in http:
            return 'PPS'
        else:
            print http
            raise ValueError('invalue site_name')

    def _get_site_url(self, http):
        if 'www.tudou.com' in http:
            return 'tudou.com'
        if 'v.youku.com' in http:
            return 'v.youku.com'
        if 'iqiyi.com' in http:
            return 'iqiyi.com'
        if 'www.hunantv.com' in http:
            return 'hunantv.com'
        if 'www.hunantv.com' in http:
            return 'hunantv.com'
        if 'www.mgtv.com' in http:
            return 'hunantv.com'
        if 'tv.sohu.com' in http:
            return 'tv.sohu.com'
        if 'www.letv.com' in http:
            return 'letv.com'
        if 'v.qq.com' in http:
            return 'v.qq.com'
        if 'vod.kankan.com' in http:
            return 'vod.kankan.com'
        if 'wasu.cn' in http:
            return 'wasu.cn'
        if 'pptv.com' in http:
            return 'v.pps.com'
        else:
            return None
            # raise ValueError('invalue site_url')

    def get_list_under_level(self, nw_url, content):
        #get url list from more
        print "nw_url:", nw_url
        next_page = re.search(r"[\w\W]*?播放源", content).group()
        su = []
        try:
            su = re.search(r'accordion\-more[\w\W]*?href=\"(.*?)\"[\w\W]*?查看全部', next_page).groups()[0]
            #需要进一步展开获取全部url
            self._push_need_deploy(su, nw_url)
        except Exception,e:
            print e
            #不需要全部展开,只需要获取当前url
            self._push_ready_made(su, content)

    def _push_need_deploy(self, su, nw_url):
        new_url = u'http://www.soku.com' + su
        print new_url
        r = requests.get(new_url, timeout=10)
        time.sleep(2)
        data = r.content
        # with open('zz2.html', 'w') as f:
        #     f.write(data)
        # next_page = re.search(r"[\w\W]*?收起", data).group()
        next_page = re.search(r'[\w\W]*?class="gotoplay"', data).group()
        infos = re.findall(r'href=\"(http.*)?".*?title=.*?\>(.*?)\<', next_page)
        # print "href_title_list:", infos
        logging.info("get %d episode" % len(infos))

        i = 0
        for info in infos:
            if '预告片' in info[1]:
                print '\tskip 预告片:', info[1]
                continue
            info_dir_vedio = {}
            url = _get_real_url(info[0])
            if not url:
                print '\tbad video url:', info[0]
                continue
            info_dir_vedio['url'] = url
            # if info_dir_vedio['url'] not in self.video_url:
            info_dir_vedio['url_hash'] = fnvhash.fnv_64a_str(info_dir_vedio['url'])
            info_dir_vedio['video_name'] = info[1]
            info_dir_vedio['video_date'] = int(re.search(r'\d{4}-\d{2}-\d{2}', info[1]).group().replace('-', ''))
            info_dir_vedio['site_hash'] = fnvhash.fnv_32a_str(self._get_site(info[0]))
            info_dir_vedio['site_url'] = self._get_site_url(info[0])
            info_dir_vedio['kid'] = self.kid
            self.do_db.item_to_table('video', info_dir_vedio)
            i += 1
        print "hit %d" % i

    def _push_ready_made(self, content):
        def get_date(content):
            _log_title = re.search('_log_title="(.*?)"', content).groups()[0]
            date_year = re.search(r'\d{4}', _log_title)
            if date_year == None:
                date_year = get_year_via_title(_log_title)
            else:
                date_year = date_year.group()
            return _log_title, date_year

        # next_page = re.search(r"[\w\W]*?播放源", content).group()
        # with open('zz6.html', 'w') as f:
        #     f.write(next_page)
        # infos = re.findall(r'date">(.*?)<[\w\W]*?ccordion\-toggle.*?href=\"(http.*)?"[\w\W]*?title="(.*?)"[\w\W]*?_log_title="(.*?)"', content)
        urls_reg = re.compile(r'<li (?:class=\'\'|class="hot"|style=\'\')[\w\W]*?href=\'(.*?)\'.*?title="(.*?)"[\w\W]*?class="reg">(.*?)<', re.S)
        infos = re.findall(urls_reg, content)
        print "zongyi count:", len(infos)

        # date_year = pub[unicode(info[3])]
        # get date
        name, date_year = get_date(content)
        temp = {}
        cnt_new = 0
        for info in infos:
            # if 'cntv.cn' in info[0] or 'ifeng.com' in info[0] or 'funshion.com' in info[0] or 'fun.tv' in info[0]:
            #     continue
            if '预告片' in info[1]:
                print '\tskip 预告片:', info[1]
                continue
            url = _get_real_url(info[0])
            if not url:
                print '\tbad video url:', info[0]
                continue
            temp['url'] = url
            temp['site_url'] = self._get_site_url(temp['url'])
            if temp['site_url'] is None:
                continue
            temp['url_hash'] = fnvhash.fnv_64a_str(temp['url'])
            print temp['url'], temp['url_hash']
            if self.do_db.is_in_table('video', 'url_hash', temp['url_hash']):
                continue
            cnt_new += 1
            if '-' in info[2]:
                m, d = info[2].split('-')
            else:
                m = info[2]
                d = ''
            date = int(date_year + m + d)
            temp['video_name'] = name + info[1].replace('&quot;', '')
            temp['video_date'] = date
            # temp['video_date'] = int(date_year + re.search(r'\d{2}-\d{2}', info[0]).group().replace('-', ''))
            temp['site_hash'] = fnvhash.fnv_32a_str(self._get_site(temp['url']))
            temp['kid'] = self.kid
            self.do_db.item_to_table('video', temp)
        print '[*] get new zy url %s' % cnt_new


    def _push_ready_made_2(self, content):
        urls_reg = '_log_title=\'(.*?)\'[\w\W]*?href="(.*?)"\>'
        # urls_reg = '\<li [class=|style=][\w\W]*?href=\'(.*?)\'.*?title="(.*?)"[\w\W]*?class="reg">(.*?)<[\w\W]*?\/li\>'
        infos = re.findall(urls_reg, content)
        print "tv_count_2:", len(infos)

        temp = {}
        cnt_new = 0
        for info in infos:
            # if 'cntv.cn' in info[0] or 'ifeng.com' in info[0] or 'funshion.com' in info[0] or 'fun.tv' in info[0]:
            #     continue
            if '预告片' in info[0]:
                print '\tskip 预告片:', info[1]
                continue
            url = _get_real_url(info[1])
            if not url:
                print '\tbad video url:', info[1]
                continue
            temp['url'] = url
            temp['site_url'] = self._get_site_url(temp['url'])
            if temp['site_url'] is None:
                continue
            temp['url_hash'] = fnvhash.fnv_64a_str(temp['url'])
            _r = self.do_db.is_in_table('video', 'url_hash', temp['url_hash'])
            if self.do_db.is_in_table('video', 'url_hash', temp['url_hash']):
                continue
            cnt_new += 1
            temp['video_name'] = info[0].replace('&quot;', '')
            temp['video_date'] = 0
            # temp['video_date'] = int(date_year + re.search(r'\d{2}-\d{2}', info[0]).group().replace('-', ''))
            temp['site_hash'] = fnvhash.fnv_32a_str(self._get_site(temp['url']))
            temp['kid'] = self.kid
            self.do_db.item_to_table('video', temp)
        print '[*] get new tv url %s' % cnt_new

    def get_zongyi_list(self, content):
        # logging.info("under_season_all")
        # self.get_list_under_level(self.second_page_url.encode('utf-8'), content)
        self._push_ready_made(content)

    def get_dianshiju_list(self, content):
        self._push_ready_made_2(content)

    def get_new_dianshiju_list(self, second_page_url):
        page = urllib.urlopen(second_page_url)
        data = page.read().decode('utf-8')
        # \u64ad\u653e\u6e90 : 播放源
        # \u7684\u76f8\u5173\u8282\u76ee :的相关节目
        # \u7efc\u5408\u6392\u5e8f : 综合排序
        resu = re.search(ur"[\w\W]*?\u7684\u76f8\u5173\u8282\u76ee", data)
        if resu == None:
            resu = re.search(ur"[\w\W]*?\u7efc\u5408\u6392\u5e8f", data)

        #print resu.group()
        next_page = resu.group()
        href_title_list = re.findall(r"href=\'http.*?\<", next_page)
        for href in href_title_list:
            m_url = self.get_url_from_href_title_dianshiju(href)
            m_num = href.split('>')[1].split('<')[0]
            self.tv_num_dir[m_url] = m_num
            #print "tv_num", self.tv_num_dir
        self.mix_list = self.mang_dir.keys()
        logging.info(self.mix_list)

    def get_tv_list_from_second_page(self):
        '''give the second_page_url, get the tv_list'''

        content = requests.get(self.second_page_url, timeout=10).content
        time.sleep(2)
        with open('zz.html', 'w') as f:
            f.write(content)
        try:
            tv_type = re.search(r'base\_type"\>(综艺)[\<\n]', content).groups()[0]
            logging.info("综艺")
            zongyi_zhida = '\xe7\xbb\xbc\xe8\x89\xba\xe7\x9b\xb4\xe8\xbe\xbe\xe5\x8c\xba \xe5\xa4\x9a\xe9\x9b\x86 BEGIN[\\w\\W]*?\xe7\xbb\xbc\xe8\x89\xba\xe7\x9b\xb4\xe8\xbe\xbe\xe5\x8c\xba \xe5\xa4\x9a\xe9\x9b\x86 END'
            zongyi_content = re.search(zongyi_zhida, content)
            zongyi_content = zongyi_content.group()
            self.get_zongyi_list(zongyi_content)
            return
        except:
            pass

        try:
            tv_type = re.search(r'base\_type"\>([\w\W].*?)[\<\n]', content).groups()[0]
        except:
            print "tv_type", self.tv_type
            return
        self.tv_type = tv_type
        print '\ttv_type:', tv_type
        if self.tv_type == '综艺':
            logging.info("综艺")
            zongyi_zhida = '\xe7\xbb\xbc\xe8\x89\xba\xe7\x9b\xb4\xe8\xbe\xbe\xe5\x8c\xba \xe5\xa4\x9a\xe9\x9b\x86 BEGIN[\\w\\W]*?\xe7\xbb\xbc\xe8\x89\xba\xe7\x9b\xb4\xe8\xbe\xbe\xe5\x8c\xba \xe5\xa4\x9a\xe9\x9b\x86 END'
            zongyi_content = re.search(zongyi_zhida, content)
            zongyi_content = zongyi_content.group()
            self.get_zongyi_list(zongyi_content)
        # elif self.tv_type == '电视剧':
        else:
            logging.info("电视剧")
            zongyi_zhida = '剧情简介[\w\W]*?关联频道信息'
            zongyi_content = re.search(zongyi_zhida, content)
            zongyi_content = zongyi_content.group()
            self.get_dianshiju_list(zongyi_content)


def start():
    time_now = time.strftime("%Y-%m-%d")
    sql = 'SELECT id, event, keywords, type FROM event_def WHERE status=0 and sdate<=\'%s\' and edate>=\'%s\' order by id ' % (time_now, time_now)
    print sql

    # sql = 'SELECT id, event, keywords, type FROM event_def WHERE keywords like "%%%%天籁之战%%%%"'
    word_dir_list = do_db.query(sql)
    logging.info("keyword num: %d" % len(word_dir_list))
    for word_dir in word_dir_list:
        id_ = word_dir['id']
        event = word_dir['event']
        name = word_dir['keywords'].encode('utf8')
        # if u'电视剧' in event or u'综艺' in event or u'—' in event:
        if word_dir['type'] != 4:
            print id_, event, name
            try:
                GetVideoUrl(name, id_=id_)
            except:
                print 'network may wrong sleep 25*60'
                time.sleep(60*10)
            time.sleep(25)
        else:
            print name, 'no'


def mession_loop():
    import time
    flag = 0
    start()
    while(1):
        st = int(time.ctime().split()[-2].split(':')[0])
        if flag and st in (8, 10, 14, 18, 22):
            start()
            flag = 0
        else:
            print "not this time and will sleep to 1h"
            time.sleep(3600)
            flag = 1


def debug(name):
    logging.info("process: %s" % name)
    GetVideoUrl(name)


if __name__ == '__main__':
    # name = '战国鬼才传'

    # ready made
    # name = '爸爸去哪儿'

    # need reploy
    # name = '天天向上'
    # name = '我是歌手'
    # name = '极限挑战'
    # name = '非诚勿扰'
    # name = '诛仙青云志'
    name = '百家姓'
    # name = '中国好声音'
    # name = '天天向上'
    # name = '快乐大本营'
    # name = '王牌对王牌'
    # name = '爱情公寓'

    from sys import argv

    if len(argv) == 1:
        mession_loop()
    else:
        debug(name)
