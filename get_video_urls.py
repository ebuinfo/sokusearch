#!/usr/bin/env python
#coding=utf-8

from selenium import webdriver
import urllib
import re
import sys
from db_info import db
import fnvhash
import time
import random
import threading
import requests
from translate_video import get_year_via_title
import logging
logging.basicConfig(level=logging.DEBUG)

from pubtime import pub
from toolkitv import IDLog

do_db = db['do']
idl = IDLog('id.log')

class GetVideoUrl(object):
    def __init__(self, tv_name):
        self.mang_dir = {}
        self.tv_type = ''
        self.tv_num_dir = {}
        self.tv_name = tv_name
        self.do_db = db['self']
        self._get_word_id()
        self._get_video_table_url()
        self.clickSouku()
        self.get_tv_list_from_second_page()

    def _get_word_id(self):
        sql = "SELECT id FROM event_def WHERE keywords='%s'" % self.tv_name
        self.craw_dir_list = self.do_db.query(sql)
        self.kid = self.craw_dir_list[0]['id']
        print 'kid:', self.kid

    def _get_video_table_url(self):
        sql = "SELECT url FROM video"
        video_url_dir = self.do_db.query(sql)
        self.video_url = [i['url'] for i in video_url_dir]

    def clickSouku(self):
        r = requests.get('http://www.soku.com/v?keyword=%s' % self.tv_name, timeout=10)
        time.sleep(2)
        self.second_page_url = r.url
        logging.info('soku_url %s' % self.second_page_url)

    def get_url_from_href_title_dianshiju(self, href):
        m = href.encode('utf-8').split("'")
        m = m[1].split('?')
        #print "m.split:",m
        self.mang_dir[m[0]] = 'true'
        return m[0]
        #print self.mang_dir.keys()
        #self.mix_list = self.mang_dir.keys()

    def _get_site(self, http):
        if 'www.tudou.com' in http:
            return '土豆'
        if 'v.youku.com' in http:
            return '优酷'
        if 'iqiyi.com' in http:
            return '爱奇艺'
        if 'www.hunantv.com' in http:
            return '芒果'
        if 'tv.sohu.com' in http:
            return '搜狐'
        if 'www.letv.com' in http:
            return '乐视'
        if 'v.qq.com' in http:
            return '腾讯'
        if 'vod.kankan.com' in http:
            return '迅雷'
        if 'wasu.cn' in http:
            return '华数'
        if 'pptv.com' in http:
            return 'PPS'
        else:
            # print 'get sit err'
            # print http
            # raise ValueError('invalue site_name')
            return

    def _get_site_url(self, http):
        if 'www.tudou.com' in http:
            return 'tudou.com'
        if 'v.youku.com' in http:
            return 'v.youku.com'
        if 'iqiyi.com' in http:
            return 'iqiyi.com'
        if 'www.hunantv.com' in http:
            return 'hunantv.com'
        if 'tv.sohu.com' in http:
            return 'tv.sohu.com'
        if 'www.letv.com' in http:
            return 'letv.com'
        if 'v.qq.com' in http:
            return 'v.qq.com'
        if 'vod.kankan.com' in http:
            return 'vod.kankan.com'
        if 'wasu.cn' in http:
            return 'wasu.cn'
        if 'pptv.com' in http:
            return 'v.pps.com'
        else:
            return
            # raise ValueError('invalue site_url')

    def get_list_under_level(self, nw_url, content):
        #get url list from more
        print "nw_url:", nw_url
        next_page = re.search(r"[\w\W]*?播放源", content).group()
        su = []
        try:
            su = re.search(r'accordion\-more[\w\W]*?href=\"(.*?)\"[\w\W]*?查看全部', next_page).groups()[0]
            #需要进一步展开获取全部url
            self._push_need_deploy(su, nw_url)
        except Exception,e:
            print e
            #不需要全部展开,只需要获取当前url
            self._push_ready_made(su, content)

    def _push_need_deploy(self, su, nw_url):
        new_url = u'http://www.soku.com' + su
        print new_url
        r = requests.get(new_url, timeout=10)
        time.sleep(2)
        data = r.content
        # with open('zz2.html', 'w') as f:
        #     f.write(data)
        # next_page = re.search(r"[\w\W]*?收起", data).group()
        next_page = re.search(r'[\w\W]*?class="gotoplay"', data).group()
        infos = re.findall(r'href=\"(http.*)?".*?title=.*?\>(.*?)\<', next_page)
        # print "href_title_list:", infos
        logging.info("get %d episode" % len(infos))

        i = 0
        for info in infos:
            info_dir_vedio = {}
            info_dir_vedio['url'] = info[0].split('?')[0]
            if info_dir_vedio['url'] not in self.video_url:
                info_dir_vedio['url_hash'] = fnvhash.fnv_64a_str(info_dir_vedio['url'])
                info_dir_vedio['video_name'] = info[1]
                info_dir_vedio['video_date'] = int(re.search(r'\d{4}-\d{2}-\d{2}', info[1]).group().replace('-', ''))
                info_dir_vedio['site_hash'] = fnvhash.fnv_32a_str(self._get_site(info[0]))
                info_dir_vedio['site_url'] = self._get_site_url(info[0])
                info_dir_vedio['kid'] = self.kid
                idd_ = self.do_db.item_to_table('video', info_dir_vedio)
                if idd_:
                    print '+',
                    sys.stdout.flush()
                    i += 1
                else:
                    print '-',
                    sys.stdout.flush()
        print "[*]hit %d" % i

    def _push_ready_made(self, content):
        def get_date(content):
            _log_title = re.search('_log_title="(.*?)"', content).groups()[0]
            date_year = re.search(r'\d{4}', _log_title)
            if date_year == None:
                _dt = None
                try:
                    _dt = re.search('更新至(.*?)"', content).groups()[0]
                    print '更新至:', _dt
                except:
                    print 'not have update untill'
                if _dt:
                    date_year = _dt[:4]
                    try:
                        int(date_year)
                    except:
                        date_year = get_year_via_title(_log_title)
                else:
                    date_year = get_year_via_title(_log_title)
            else:
                date_year = date_year.group()
            return _log_title, date_year

        def _get_real_url(url):
            return url.split('?')[0]
        # next_page = re.search(r"[\w\W]*?播放源", content).group()
        # with open('zz6.html', 'w') as f:
        #     f.write(next_page)
        # infos = re.findall(r'date">(.*?)<[\w\W]*?ccordion\-toggle.*?href=\"(http.*)?"[\w\W]*?title="(.*?)"[\w\W]*?_log_title="(.*?)"', content)
        urls_reg = '\<li [class=|style=][\w\W]*?href=\'(.*?)\'.*?title="(.*?)"[\w\W]*?class="reg">(.*?)<[\w\W]*?\/li\>'
        infos = re.findall(urls_reg, content)
        print "href_title_list:", len(infos)
        logging.info("get %d" % len(infos))

        # date_year = pub[unicode(info[3])]
        # get date
        name, date_year = get_date(content)
        temp = {}
        for info in infos:
            # if 'cntv.cn' in info[0] or 'ifeng.com' in info[0] or 'funshion.com' in info[0] or 'fun.tv' in info[0]:
            #     continue
            temp['url'] = _get_real_url(info[0])
            temp['site_url'] = self._get_site_url(temp['url'])
            if temp['site_url'] is None:
                continue
            if '-' in info[2]:
                m, d = info[2].split('-')
            else:
                m = info[2]
                d = ''
            date = int(date_year + m + d)
            temp['url_hash'] = fnvhash.fnv_64a_str(temp['url'])
            temp['video_name'] = name + info[1].replace('&quot;', '')
            temp['video_date'] = date
            # temp['video_date'] = int(date_year + re.search(r'\d{2}-\d{2}', info[0]).group().replace('-', ''))
            temp['site_hash'] = fnvhash.fnv_32a_str(self._get_site(temp['url']))
            temp['kid'] = self.kid
            id_ = self.do_db.item_to_table('video', temp)
            if id_:
                print '+',
                sys.stdout.flush()
            else:
                print '.',
                sys.stdout.flush()


    def get_zongyi_list(self, content):
        # logging.info("under_season_all")
        # self.get_list_under_level(self.second_page_url.encode('utf-8'), content)
        self._push_ready_made(content)

    def get_new_dianshiju_list(self, data):
        # \u64ad\u653e\u6e90 : 播放源
        # \u7684\u76f8\u5173\u8282\u76ee :的相关节目
        # \u7efc\u5408\u6392\u5e8f : 综合排序
        # resu = re.search(ur"[\w\W]*?\u7684\u76f8\u5173\u8282\u76ee", data)
        print type(data)
        resu = re.search("[\w\W]*?关联频道信息", data)
        if resu == None:
            print '[*] try re 综合排序'
            resu = re.search("[\w\W]*?综合排序", data)
            # resu = re.search(ur"[\w\W]*?\u7efc\u5408\u6392\u5e8f", data)

        #print resu.group()
        with open('rrrdata.html', 'w') as f:
            f.write(data)
        next_page = resu.group()
        with open('rrr.html', 'w') as f:
            f.write(next_page)
        href_title_list = re.findall(r"href=\"(http.*?)\"", next_page)
        print len(href_title_list)
        for href in href_title_list:
            temp = {}
            temp['url'] = href.split('?')[0]
            try:
                temp['site_hash'] = fnvhash.fnv_32a_str(self._get_site(temp['url']))
                temp['site_url'] = self._get_site_url(temp['url'])
            except:
                continue
            temp['url_hash'] = fnvhash.fnv_64a_str(temp['url'])
            temp['video_name'] = self.tv_name
            temp['video_date'] = '00000000'
            # temp['video_date'] = int(date_year + re.search(r'\d{2}-\d{2}', info[0]).group().replace('-', ''))
            temp['kid'] = self.kid
            id_ = self.do_db.item_to_table('video', temp)
            if id_:
                print '+',
                sys.stdout.flush()
            else:
                print '.',
                sys.stdout.flush()

    def get_tv_list_from_second_page(self):
        '''give the second_page_url, get the tv_list'''

        content = requests.get(self.second_page_url, timeout=10).content
        time.sleep(2)
        with open('zz.html', 'w') as f:
            f.write(content)
        zongyi_zhida = u'\xe7\xbb\xbc\xe8\x89\xba\xe7\x9b\xb4\xe8\xbe\xbe\xe5\x8c\xba \xe5\xa4\x9a\xe9\x9b\x86 BEGIN[\w\W]*?\xe7\xbb\xbc\xe8\x89\xba\xe7\x9b\xb4\xe8\xbe\xbe\xe5\x8c\xba \xe5\xa4\x9a\xe9\x9b\x86 END'
        zongyi_content = re.search(zongyi_zhida, content)
        if zongyi_content == None:
            # this please should add tv_content
            print 'tv url'
            self.get_new_dianshiju_list(content)
            return
        else:
            zongyi_content = zongyi_content.group()
        tv_type = re.search(r'base\_type"\>([\w\W].*?)[\<\n]', zongyi_content).groups()[0]
        self.tv_type = tv_type
        logging.info("综艺")
        self.get_zongyi_list(zongyi_content)


def start(start_keyword=None):
    iid = idl.get_id()
    while 1:
        sql = 'SELECT id, keywords FROM event_def WHERE status = 0 and id > %s limit 2' % iid
        wl = do_db.query(sql)
        if len(wl) < 2:
            idl.save_id(125)
        try:
            iid = wl[0]['id']
        except:
            return
        name = wl[0]['keywords']
        print '%%%', iid, name
        if start_keyword:
            if name != start_keyword.decode('utf8'):
                continue
        logging.info("process: %s" % name)
        cc = GetVideoUrl(name)
        idl.save_id(iid)
        start_keyword = None
        print 'to next key'
        time.sleep(20)


def mession_loop(start_keyword=None):
    import time
    flag = 1
    while(1):
        start(start_keyword=start_keyword)
        time.sleep(3600)
        start_keyword = None


def debug(name):
    logging.info("process: %s" % name)
    cc = GetVideoUrl(name)


if __name__ == '__main__':
    # name = '爱情公寓'

    from sys import argv

    if len(argv) == 1:
        mession_loop()
    elif len(argv) == 2:
        mession_loop(start_keyword=argv[1])
    else:
        debug(argv[1])
