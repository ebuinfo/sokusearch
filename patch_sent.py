from sentiment_weibo import get_sentiment_danmu
from db_info import db_
db = db_
import toolkitv




def patch_sent(d):
    sentiment = get_sentiment_danmu(d['content'])
    # print 'sentiment', sentiment ##
    print d['id'], sentiment
    sql = 'update video_comment set sentiment=%s where id=%s' % (sentiment, d['id'])
    db.execute(sql)



def get_data(minid, maxid, idlog=None):
    while 1:
        id_ = idlog.get_id()
        if id_ < minid:
            id_ = minid
        sql = 'select id, content from video_comment where id>%s and id<%s limit 100' % (id_, maxid) ##
        d = db.query(sql)
        if d == []:
            print 'down'
            exit() ##
        yield d
        idlog.save_id(id_+100) ##


def main(opt, idlog=None):
    for ds in get_data(int(opt.minid), int(opt.maxid), idlog=idlog):
        for d in ds:
            if not d:
                print 'down'
                exit() ##
            else:
                patch_sent(d)


if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-a", "--min", dest="minid", help="time to start")
    parser.add_option("-z", "--max", dest="maxid", help="time to start")
    (opt, args) = parser.parse_args()
    logfn = 'sent_idlog_' + str(opt.minid) + '_' + str(opt.maxid)
    idlog = toolkitv.IDLog(logfn)
    main(opt, idlog=idlog)

