#!/usr/bin/env python
# encoding=utf8


import re
import time


def get_year_via_title(title):
    if title == '中国好声音 第四季':
        return '2015'
    if title == '中国梦想秀 第九季':
        return '2016'
    if title == '中国最强音 第一季':
        return '2013'
    if title == '中国梦之声 第二季':
        return '2014'
    if title == '笑傲江湖 第二季':
        return '2015'
    if title == '我的中国星 第一季':
        return '2013'
    if title == '人生第一次 第二季':
        return '2013'
    if title == '我不是明星 第七季':
        return '2015'
    if title == '最强大脑 第三季':
        return '2016'
    if title == '我不是明星 第七季':
        return '2015'
    if title == '关爱八卦成长协会 第一季':
        return '2016'
    if title == '奔跑吧兄弟 第三季':
        return '2016'
    if title == '奔跑吧兄弟 第四季':
        return '2016'
    if title == '爸爸 去哪儿 第三季':
        return '2015'
    if title == '极限挑战 第二季':
        return '2016'
    if title == '我为喜剧狂 第三季':
        return '2016'
    if title == '天才想得到 第二季':
        return '2015'
    if title == '欢乐饭米粒儿 第一季':
        return '2015'
    if title == '音乐大师课 第二季':
        return '2016'
    if title == '诗歌之王二 第一季':
        return '2016'
    if title == '看见你的声音 第二季':
        return '2016'
    if title == '一路上有你 第二季':
        return '2016'
    if title == '急诊室故事 第二季':
        return '2016'


    else:
        year = re.search(r'\d{4}', time.ctime()).group()
        return year
        # raise Exception('need patch year-title')
