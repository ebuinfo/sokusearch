#!/usr/bin/python
# encoding=utf8


import requests
import re
import time
import urllib
from selenium import webdriver


def dive_into_mangguo(url):

    # service_args = ['--load-images=false']
    # self_driver = webdriver.PhantomJS(service_args=service_args)
    # self_driver.set_page_load_timeout(10)

    # page = urllib.urlopen(url)
    # page_data = page.read()
    # reg = r'release_date\: ".*?\"'
    # #reg = r'release_date.*?\"\,'
    # #reg = r'window.VI[\w\W]+?\}'
    # imgre = re.compile(reg)
    # imglist = re.search(imgre, page_data).group()
    # m = imglist.split('"')
    # try:
    #     self_driver.get(url)
    #     time.sleep(2)
    # except Exception:
    #     pass
    r = requests.get(url, timeout=5)
    time.sleep(2)
    if r.status_code == 404:
        return

    vid = re.search(r'f/(\d+?)\.html', url).groups()[0]

    co_url = 'http://videocenter-2039197532.cn-north-1.elb.amazonaws.com.cn//dynamicinfo?vid=%s' % vid
    r1 = requests.get(co_url)
    result = eval(r1.content)
    play = result['data']['all']
    up = result['data']['like']
    down = result['data']['unlike']

    com_url = 'http://comment.hunantv.com/video_comment/list/?subject_id=%s' % vid
    r2 = requests.get(com_url)
    result2 = eval(r2.content)
    comment = result2['total_number']
    ret = {'url':url, 'play':play, 'comment':comment, 'up':up, 'down':down}
    # print str(ret)
    return ret

    # comment = self_driver.find_element_by_class_name('comments-total-nums').text
    # up = self_driver.find_element_by_class_name('eln-1').text
    # down = self_driver.find_element_by_class_name('eln-2').text
    # play_temp = self_driver.find_element_by_class_name('play-numall').text.split()[0]

    # c_dir = {u'万':10000, u'亿':100000000, ',':1}
    # resu = re.search(r'[0-9]*', play_temp)
    # a = int(resu.group())
    # d = a
    # su = re.search(r',', play_temp)
    # if su:
    #     m = play_temp.split(',')
    #     d = int(m[0]) * 1000 + int(m[1])
    # else:
    #     b = re.search(r'[^\d]', play_temp).group()
    #     if b == u'.':
    #         bb = play_temp.split('.')[1]
    #         b = re.search(r'[^\d]', bb).group()
    #         a = play_temp.split(b)[0]
    #     d = float(a) * c_dir[b]

