#!/usr/bin/env python
#coding=utf-8

import re
from db_info import db
import fnvhash
import time
import json
import urllib
import requests
import logging
logging.basicConfig(level=logging.INFO)


do_db = db['do']

class GetVideoUrl(object):
    def __init__(self, tv_name, id_=None):
        self.mang_dir = {}
        self.tv_type = ''
        self.tv_num_dir = {}
        self.tv_name = tv_name
        self.do_db = db['self']
        if id_ == None:
            self._get_word_id()
        else:
            self.kid = id_

        # self._get_video_table_url()
        self.clickSouku()
        self.get_tv_list_from_second_page()

    def _get_word_id(self):
        sql = "SELECT id FROM event_def WHERE keywords='%s'" % self.tv_name
        self.craw_dir_list = self.do_db.query(sql)
        self.kid = self.craw_dir_list[0]['id']

    def _get_video_table_url(self):
        sql = "SELECT url FROM video"
        video_url_dir = self.do_db.query(sql)
        self.video_url = [i['url'] for i in video_url_dir]

    def clickSouku(self):
        url = 'http://v.qq.com/x/search/?q='
        m = 2
        success = False
        while m >= 0:
            try:
                r = requests.get( url + urllib.quote(self.tv_name), timeout=10)
                if r.status_code == 200:
                    success = True
                    html = r.content
                    break
                elif r.status_code == 404:
                    self.second_page_url = ''
                    return
                else:
                    m -= 1
                    time.sleep(3)
            except:
                m -= 1
                time.sleep(3)
        if not success:
            self.second_page_url = ''
            return
        trx = re.compile(r'.*?<em class="hl">(?P<video_name>.*?)</em>.*?<span class="type">(?P<tv_type>.*?)</span>', re.S)
        mm = re.match(trx, html)
        if mm:
            tt = mm.groupdict()
            self.video_name = tt['video_name']
            self.tv_type = tt['tv_type']
        else:
            self.second_page_url = ''
            return
        rx = re.compile(r".*?r-props=.\{\s*id: '(?P<id>\w*?)'[;\s]*dataType: '(?P<data_type>\d*?)'[;\s]*videoType: '(?P<video_type>\d*?)';.*?activeRange: '(?P<range>[-\d]*?)';.*?title", re.S)
        m = re.match(rx, html)
        if m:
            base = 'http://s.video.qq.com/get_playsource?'
            d = m.groupdict()
            # print d
            # raise Exception('deliberately')
            d['plat'] = '2'
            d['type'] = '4'
            d['plname'] = 'qq'
            d['otype'] = 'json'
            self.second_page_url = base + urllib.urlencode(d)
        else:
            self.second_page_url = ''

    def get_url_from_href_title_dianshiju(self, href):
        m = href.encode('utf-8').split("'")
        m = m[1].split('?')
        #print "m.split:",m
        self.mang_dir[m[0]] = 'true'
        return m[0]
        #print self.mang_dir.keys()
        #self.mix_list = self.mang_dir.keys()

    def _get_site(self, http):
        if 'www.tudou.com' in http:
            return '土豆'
        if 'v.youku.com' in http:
            return '优酷'
        if 'iqiyi.com' in http:
            return '爱奇艺'
        if 'www.hunantv.com' in http:
            return '芒果'
        if 'www.mgtv.com' in http:
            return '芒果'
        if 'tv.sohu.com' in http:
            return '搜狐'
        if 'www.letv.com' in http:
            return '乐视'
        if 'v.qq.com' in http:
            return '腾讯'
        if 'vod.kankan.com' in http:
            return '迅雷'
        if 'wasu.cn' in http:
            return '华数'
        if 'pptv.com' in http:
            return 'PPS'
        else:
            print http
            raise ValueError('invalue site_name')

    def _get_site_url(self, http):
        if 'www.tudou.com' in http:
            return 'tudou.com'
        if 'v.youku.com' in http:
            return 'v.youku.com'
        if 'iqiyi.com' in http:
            return 'iqiyi.com'
        if 'www.hunantv.com' in http:
            return 'hunantv.com'
        if 'www.hunantv.com' in http:
            return 'hunantv.com'
        if 'www.mgtv.com' in http:
            return 'hunantv.com'
        if 'tv.sohu.com' in http:
            return 'tv.sohu.com'
        if 'www.letv.com' in http:
            return 'letv.com'
        if 'v.qq.com' in http:
            return 'v.qq.com'
        if 'vod.kankan.com' in http:
            return 'vod.kankan.com'
        if 'wasu.cn' in http:
            return 'wasu.cn'
        if 'pptv.com' in http:
            return 'v.pps.com'
        else:
            return None
            # raise ValueError('invalue site_url')

    def get_zongyi_list(self, content):
        def get_date(title):
            a = re.search('(\d{4})-(\d{2})-(\d{2})', title).group()
            return int(a.replace('-', ''))

        def _get_real_url(url):
            return url.split('?')[0]
        urls_reg = r'href="(http://v.qq.com/cover.*?)" targe.*?title="(.*?)".*?>'
        infos = re.findall(urls_reg, content)
        print "zongyi count:", len(infos)

        # date_year = pub[unicode(info[3])]
        # get date
        temp = {}
        cnt_new = 0
        for info in infos:
            # if 'cntv.cn' in info[0] or 'ifeng.com' in info[0] or 'funshion.com' in info[0] or 'fun.tv' in info[0]:
            #     continue
            temp['url'] = _get_real_url(info[0])
            temp['site_url'] = self._get_site_url(temp['url'])
            if temp['site_url'] is None:
                continue
            temp['url_hash'] = fnvhash.fnv_64a_str(temp['url'])
            if self.do_db.is_in_table('video', 'url_hash', temp['url_hash']):
                continue
            cnt_new += 1
            temp['video_name'] = info[1].replace('&quot;', '')
            temp['video_date'] = get_date(temp['video_name'])
            # temp['video_date'] = int(date_year + re.search(r'\d{2}-\d{2}', info[0]).group().replace('-', ''))
            temp['site_hash'] = fnvhash.fnv_32a_str(self._get_site(temp['url']))
            temp['kid'] = self.kid
            self.do_db.item_to_table('video', temp)
        print '[*] get new zy url %s' % cnt_new


    def get_dianshiju_list(self, content):
        if content == '':
            return
        rx = re.compile(r'.*=(\{.*\});')
        m = re.match(rx, content)
        if not m:
            return
        d = json.loads(m.groups()[0])
        if d['error']:
            return
        infos = d['PlaylistItem']['videoPlayList']
        temp = {}
        cnt_new = 0
        for i in infos:
            temp['url'] = i['playUrl'].encode('utf8')
            temp['url_hash'] = fnvhash.fnv_64a_str(temp['url'])
            r = self.do_db.get('SELECT * FROM video WHERE url_hash=%s', temp['url_hash'])
            if r:
                continue
            cnt_new += 1
            temp['vid'] = i['id']
            temp['site_url'] = self._get_site_url(temp['url'])
            temp['site_hash'] = fnvhash.fnv_32a_str(self._get_site(temp['url']))
            temp['kid'] = self.kid
            temp['video_name'] = self.video_name
            temp['video_date'] = 0
            self.do_db.item_to_table('video', temp)


    def get_tv_list_from_second_page(self):
        '''give the second_page_url, get the tv_list'''

        logging.info('catch url %s' % self.second_page_url)

        if self.second_page_url == '':
            return
        content = ''
        m = 2
        while m >= 0:
            try:
                r = requests.get(self.second_page_url, timeout=10)
                if r.status_code == 200:
                    content = r.content
                    break
                elif r.status_code == 404:
                    break
                else:
                    m -= 1
                    time.sleep(3)
            except:
                m -= 1
                time.sleep(3)
        time.sleep(2)
        with open('qq.html', 'w') as f:
            f.write(content)
        if self.tv_type == '综艺':
            self.get_zongyi_list(content)
        elif self.tv_type == '生活':
            self.get_zongyi_list(content)
        elif self.tv_type == '电视剧':
            self.get_dianshiju_list(content)


def start():
    time_now = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    sql = 'SELECT id, event, keywords FROM event_def WHERE status=0 and \'%s\'>sdate and \'%s\'<edate and id>485' % (time_now, time_now)
    print sql

    # sql = 'SELECT id, event, keywords FROM event_def WHERE status=0 and id>=155'
    word_dir_list = do_db.query(sql)
    logging.info("keyword num: %d" % len(word_dir_list))
    for word_dir in word_dir_list:
        id_ = word_dir['id']
        event = word_dir['event']
        name = word_dir['keywords'].encode('utf8')
        print id_
        if u'电视剧' in event or u'综艺' in event:
            print id_, event, name
            GetVideoUrl(name, id_=id_)
            time.sleep(25)
        else:
            print name, 'no'


def mession_loop():
    import time
    flag = 1
    while(1):
        st = int(time.ctime().split()[-2].split(':')[0])
        if flag and (st == 9):
            start()
            flag = 0
        else:
            print "not this time and will sleep to 1h"
            time.sleep(3600)
            flag = 1


def debug(name):
    logging.info("process: %s" % name)
    GetVideoUrl(name)


if __name__ == '__main__':
    # name = '战国鬼才传'

    # ready made
    # name = '爸爸去哪儿'

    # need reploy
    # name = '天天向上'
    # name = '我是歌手'
    # name = '极限挑战'
    # name = '非诚勿扰'
    # name = '诛仙青云志'
    # name = '百家姓'
    # name = '中国好声音'
    # name = '天天向上'
    # name = '快乐大本营'
    # name = '王牌对王牌'
    # name = '爱情公寓'

    from sys import argv

    if len(argv) == 1:
        mession_loop()
    else:
        debug(argv[1])
