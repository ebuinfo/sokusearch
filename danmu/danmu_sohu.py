#!/usr/bin/python
# encoding=utf8


import requests
import json
import time
import sys
sys.path.append('../')
import fnvhash
import re
from xml.etree import ElementTree
from danmu_config import db
from sentiment_weibo import get_sentiment_danmu
# db = MySQLUtility('localhost', 'video', 'root', 'root')


def get_vid(url):
    # sql = 'select vid from video where url_hash=%s' % url_hash
    # return db.query(sql)[0]['vid']
    r = requests.get(url)
    vid = re.search(r'var vid="(\d*?)"', r.content).groups()[0]
    return vid



def get_danmu(url):
    print '\n' + '-------------' * 3
    print url
    url_hash = fnvhash.fnv_64a_str(url)
    # print url
    vid = get_vid(url)
    # r = get_danm_1(url, url_hash, vid)
    # if r:
    #     return
    # get_danmu_2(url, url_hash, vid)
    get_danmu_3(url, url_hash, vid)

def get_danm_1(url, url_hash, vid):
    ru = 'http://cdn.danmu.56.com/xml/2/v_%s.xml' % vid
    print ru
    r = requests.get(ru, timeout=10)
    time.sleep(1)
    if r.status_code == 404:
        print 'sohu danmu  should try another way'
        return
    root = ElementTree.fromstring(r.content)
    node_list = root.getiterator('d')[0]
    return push_danmu(node_list, url_hash)


def get_danmu_2(url, url_hash, vid):
    aid = get_aid(url)
    du = 'http://danmu.56.com/danmu?act=dmlist&vid=%s&page=1&pct=2&request_from=sohu_vrs_player&o=4&aid=%s' % (vid, aid)
    print du
    r = requests.get(du, timeout=10)
    time.sleep(1)
    result = json.loads(r.content)
    dfd = result[u'info'][u'dfd']
    i = 0
    j = 0
    for m in result[u'info'][u'comments']:
        d = {}
        d['url_hash'] = url_hash
        d['content'] = m[u'c']
        d['pubtime'] = dfd - m[u'dd']
        d['content_hash'] = fnvhash.fnv_32a_str(d['content'].encode('utf8'))
        d['sentiment'] = get_sentiment_danmu(d['content'])
        lid = db.item_to_table('danmu', d)
        if lid:
            print '+',
            sys.stdout.flush()
            i += 1
            j = 0
        else:
            print '.',
            sys.stdout.flush()
            j += 1
            if j >= 15:
                print i
                return
    print i


def get_danmu_3(url, url_hash, vid):
    aid = get_aid(url)
    du = 'http://api.danmu.tv.sohu.com/danmu?act=dmlist_v2&dct=1&vid=%s&page=1&pct=2&request_from=sohu_vrs_player&o=4&aid=%s' % (vid, aid)
    print du
    r = requests.get(du, timeout=10)
    time.sleep(1)
    result = json.loads(r.content)
    dfd = result[u'info'][u'dfd']
    i = 0
    j = 0
    for m in result[u'info'][u'comments']:
        d = {}
        d['url_hash'] = url_hash
        d['content'] = m[u'c']
        d['pubtime'] = dfd - m[u'dd']
        d['content_hash'] = fnvhash.fnv_32a_str(d['content'].encode('utf8'))
        d['sentiment'] = get_sentiment_danmu(d['content'])
        lid = db.item_to_table('danmu', d)
        if lid:
            print '+',
            sys.stdout.flush()
            i += 1
            j = 0
        else:
            print '.',
            sys.stdout.flush()
            j += 1
            if j >= 15:
                print i
                return
    print i



def get_aid(url):
    r = requests.get(url)
    time.sleep(1)
    return re.search(r'var playlistId="(.*?)"', r.content).groups()[0]


def push_danmu(node_list, url_hash):
    i = 0
    j = 0
    for node in node_list:
        ttime = int(float(node.get('d')))
        content = node.text
        d = {}
        d['url_hash'] = url_hash
        d['content'] = content
        d['content_hash'] = fnvhash.fnv_32a_str(d['content'].encode('utf8'))
        d['sentiment'] = get_sentiment_danmu(d['content'])
        d['pubtime'] = ttime
        id_ = db.item_to_table('danmu', d)
        if id_:
            print '+',
            sys.stdout.flush()
            i += 1
            j = 0
        else:
            print '.',
            sys.stdout.flush()
            j += 1
            if j >= 15:
                return
    print i
    return i


def already_in_table(ttime, content, url_hash):
    time.sleep(0.05)
    # dan['content'] = dan['content'].replace('"', '')
    content_hash = fnvhash.fnv_32a_str(content.encode('utf8'))
    sql = 'SELECT id FROM danmu WHERE pubtime=%s and url_hash=%s and content_hash="%s"' % (ttime, url_hash, content_hash)
    print sql
    d = db.query(sql)
    try:
        d = db.query(sql)
    except:
        print sql
        raise()
    # d = db.query(sql)[0]['id']
    print 'd:', len(d)
    if d == []:
        return False
    return True



def patch_vid(url):
    url_hash = fnvhash.fnv_64a_str(url)
    r = requests.get(url, timeout=10)
    time.sleep(1)
    # var vid="2816456";
    vid = re.search('var vid="(.*?)"', r.content).groups()[0]
    sql = "UPDATE video SET vid=%s WHERE url_hash=%s" % (vid, url_hash)
    print sql
    db.execute(sql)


if __name__ == '__main__':
    # url = 'http://tv.sohu.com/20160221/n438031618.shtml'
    # url = 'http://tv.sohu.com/20160611/n453908413.shtml'
    url = 'http://tv.sohu.com/20160618/n455097229.shtml'
    get_danmu(url)
