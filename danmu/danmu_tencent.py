#!/usr/bin/python
# encoding=utf8


import requests
import json
import time
import sys
sys.path.append('../')
import fnvhash
import re
from danmu_config import db
from MySQLdb import _mysql
# db = MySQLUtility('localhost', 'video', 'root', 'root')


def get_targetid(url):
    try:
        r = requests.get(url, timeout = 10)
    except:
        pass
    time.sleep(1)
    # updateVid="v0019kvrwjo"
    with open('zzqq.html', 'w') as f:
        f.write(r.content)
    _id = re.search(r'vid: *"(.*?)"', r.content).groups()[0]
    u2 = 'http://sns.video.qq.com/fcgi-bin/video_comment_id?otype=json&op=3&vid=%s' % (_id)
    r2 = requests.get(u2)
    com_id = re.search('comment_id":"(.*?)"', r2.content).groups()[0]
    return com_id


def get_danmu(url):
    url = url.replace('https', 'http')
    url_hash = fnvhash.fnv_64a_str(url)
    print url
    targetid = get_targetid(url)
    print "targetid:", targetid

    my_headers = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36'}
    last_id = 6196003163797533087
    while 1:
        danmu_url = 'http://coral.qq.com/article/%s/comment?commentid=%s&reqnum=50' % (targetid, last_id)
        print danmu_url
        dr = requests.get(danmu_url, headers=my_headers, timeout=10)
        danmu = json.loads(dr.content)
        last_id = danmu['data']['last']
        print '\nfirst:', danmu['data']['first']
        print 'last:', danmu['data']['last']
        result = danmu['data']['commentid']
        if not result:
            print 'no result any more'
            break
        print result
        push_danmu(result, url_hash)
        time.sleep(10)


def filter_emoji(desstr,restr=''):
    '''
    过滤表情
    '''
    try:
        co = re.compile(u'[\U00010000-\U0010ffff]')
    except re.error:
        co = re.compile(u'[\uD800-\uDBFF][\uDC00-\uDFFF]')
    return co.sub(restr, desstr)


def push_danmu(result, url_hash):
    i = 0
    for dan in result:
        dan['content'] = filter_emoji(dan['content'])
        if already_in_table(dan, url_hash):
            print '.',
            sys.stdout.flush()
            # return
        else:
            handle_and_push(dan, url_hash)
            sys.stdout.flush()
            i += 1
    print i


def already_in_table(dan, url_hash):
    time.sleep(0.05)
    ct = dan['time']
    # dan['content'] = dan['content'].replace('"', '')
    # content_hash = fnvhash.fnv_32a_str(dan['content'].encode('utf8'))
    content_hash = dan['id']
    sql = 'SELECT id FROM danmu WHERE pubtime=%s and url_hash=%s and content_hash="%s"' % (ct, url_hash, content_hash)
    # print sql
    d = db.query(sql)
    try:
        d = db.query(sql)
    except:
        print sql
        # raise()
    # d = db.query(sql)[0]['id']
    # print sql, str(d)
    if d == []:
        return False
    return True


def handle_and_push(dan, url_hash):
    d = {}
    d['url_hash'] = url_hash
    d['content'] = dan['content']
    d['content_hash'] = fnvhash.fnv_32a_str(d['content'].encode('utf8'))
    # d['content_hash'] = dan['id']
    d['pubtime'] = dan['time']
    try:
        db.item_to_table('danmu', d)
        print '+',
    except _mysql.OperationalError:
        print '!',


def patch_vid(url):
    url_hash = fnvhash.fnv_64a_str(url)
    print url_hash
    r = requests.get(url, timeout=10)
    time.sleep(1)
    # var videoId = '368632305';
    vid = re.search('var videoId = \'(.*?)\'', r.content).groups()[0]
    sql = "UPDATE video SET vid=%s WHERE url_hash=%s" % (vid, url_hash)
    db.execute(sql)


if __name__ == '__main__':
    # url = 'http://v.qq.com/cover/o/omcczahf3t2sye4/j0019oe3c5r.html'
    url = 'http://v.qq.com/x/cover/r009isnukiff1ee.html'
    url = 'https://v.qq.com/x/cover/sg4tqh6z95s5lv3.html?vid=r0021w1avyj'
    url = 'http://v.qq.com/cover/4/4e4r3amfp1qzppm.html?vid=q0021qqvz1e'

    get_danmu(url)
