#!/usr/bin/python
# encoding=utf8


from gevent import monkey; monkey.patch_all()
import gevent
import requests
import json
import time
import random
import sys
sys.path.append('../')
import fnvhash
from sentiment_weibo import get_sentiment_danmu
import re
from danmu_config import db
# db = MySQLUtility('localhost', 'video', 'root', 'root')


def get_vid(url):
    r = requests.get(url)
    #with open('zz.html', 'w') as f:
    #    f.write(r.content)
    vid_ = re.search('videoId = \'(.*?)\'', r.content)
    if vid_ == None:
        vid_ = re.search('videoId:\"(.*?)\"', r.content)
        print 'vid_2:', vid_
    try:
        vid = vid_.groups()[0]
        return vid
    except:
        print 'some wrong with find vid '
        # exit()



def get_danmu(url):
    url_hash = fnvhash.fnv_64a_str(url)
    print url
    vid = get_vid(url)
    print "vid:", vid
    for mat in range(0, 500):
        rand = random.randint(1200, 9000)
        danmu_url = 'http://service.danmu.youku.com/list?{}'.format(rand)
        dat = {
            'mcount': '1',
            'mat': str(mat),
            'iid': str(vid),
            'type': '1',
            'ct': '1001',
            'uid': '0',
            # 'ouid': str(owner_uid),
        }
        print danmu_url, dat
        dr = requests.post(danmu_url, data=dat, timeout=10)
        # gevent.sleep(1)
        try:
            open('z-danmu-youku.json', 'w').write(dr.content)
            danmu = json.loads(dr.content)
        except:
            return
        count = danmu['count']
        if count == 0:
            print 'count is 0, mat is %s, should exit' % mat
            return
        print "count", count, ', mat: ', mat
        r = push_danmu(danmu['result'], url_hash)
        gevent.sleep(4.2)
        if r == -1:
            return
        # print danmu['count']


def filter_emoji(desstr,restr=''):
    '''
    过滤表情
    '''
    try:
        co = re.compile(u'[\U00010000-\U0010ffff]')
    except re.error:
        co = re.compile(u'[\uD800-\uDBFF][\uDC00-\uDFFF]')
    return co.sub(restr, desstr)



def push_danmu(result, url_hash):
    i = 0
    j = 0
    for dan in result:
        dc = int(dan['createtime'])
        if dc > 10000000000:
            pubtime = dc/1000
        else:
            pubtime = dc
        content_hash = fnvhash.fnv_32a_str(dan['content'].encode('utf8'))
        sql = 'select id from danmu where url_hash=%s and content_hash=%s and pubtime=%s limit 1' % (url_hash, content_hash, pubtime)
        # print sql
        z = db.get(sql)
        if z:
            # sql = 'update danmu set show_offset=%s where id=%s' % (dan['playat'], z['id'])
            # print sql
            # db.execute(sql)
            continue
        d = {}
        d['content_hash'] = content_hash
        d['url_hash'] = url_hash
        d['show_offset'] = dan['playat']
        d['content'] = filter_emoji(dan['content'])
        d['sentiment'] = get_sentiment_danmu(d['content'])
        d['pubtime'] = pubtime
        _id = db.item_to_table('danmu', d)
        if _id:
            print '+',
            sys.stdout.flush()
            i += 1
            j = 0
        else:
            print '.',
            sys.stdout.flush()
            j += 1
            if j >= 15:
                print i
                return -1
    print i


if __name__ == '__main__':
    # url ='http://v.youku.com/v_show/id_XMTQ2NjIyMzg2OA==.html'
    # url = 'http://v.youku.com/v_show/id_XMTU0ODEzMDI0OA==.html'
    # vid = get_vid(url)
    #url = 'http://v.youku.com/v_show/id_XMTc2NjczMzE2NA==.html'
    #url = 'http://v.youku.com/v_show/id_XMTg4ODE4MDA2OA==.html'
    #url = 'http://v.youku.com/v_show/id_XMTg1OTMzNjIxMg==.html'
    #url = 'http://v.youku.com/v_show/id_XMjMwMDM2OTE2NA==.html'
    # url = 'http://v.youku.com/v_show/id_XMjQ3NDY4MjgzMg==.html'
    # url = 'http://v.youku.com/v_show/id_XMTg0MzExNjIzMg==.html'
    url = 'http://v.youku.com/v_show/id_XMTg4ODE4MDA2OA==.html'
    get_danmu(url)
