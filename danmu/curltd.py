#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Module name
# Create on Sat, Jan 21 2017

from collections import namedtuple
import gzip
from io import BytesIO

import pycurl

Response = namedtuple('Response', 'status_code content url')

def get_tudou_page(url):
    buf = BytesIO()
    c = pycurl.Curl()
    c.setopt(pycurl.URL, url)
    c.setopt(pycurl.FOLLOWLOCATION, 1)
    c.setopt(pycurl.MAXREDIRS, 4)
    c.setopt(pycurl.CONNECTTIMEOUT, 80)
    c.setopt(pycurl.HTTPHEADER, [
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding: gzip,deflate',
        'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 '
        '(KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36'
    ])
    c.setopt(pycurl.WRITEFUNCTION, buf.write)
    # if use_proxy == True:
    #     c.setopt(pycurl.PROXY, SSERVER)
    try:
        c.perform()
        status_code = c.getinfo(c.HTTP_CODE)
        if status_code == 200:
            buf.seek(0)
            zf = gzip.GzipFile(mode='rb', fileobj=buf)
            html = zf.read()
            with open('ztd.html', 'w') as f:
                f.write(html)
            zf.close()
        else:
            html = ''
        uu = url.decode('utf-8')
        r = Response(status_code, html, uu)
        return r
    except Exception as e:
        traceback.print_exc()
        raise e

if __name__ == '__main__':
    url = 'http://www.tudou.com/albumplay/lPBkww_XLN8/oMDT3Cn7pKk.html'
    #url = 'http://www.tudou.com/albumplay/0GdzKW18i8M/94V7T2RU1Ng.html'
    get_tudou_page(url)
