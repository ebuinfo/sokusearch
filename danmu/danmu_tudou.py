#!/usr/bin/python
# encoding=utf8

from gevent import monkey; monkey.patch_all()
import gevent

import sys
sys.path.append('../')
import requests
import re
from sentiment_weibo import get_sentiment_danmu
import json
import time
import random
from danmu_config import db
import fnvhash
import traceback

# from curltd import get_tudou_page

def get_vid(url):
    # r = get_tudou_page(url)
    # with open('ztd.html', 'w') as f:
    #     f.write(r.content)
    r = requests.get(url, headers=
            { 'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36' }
    )

    try:
        # vid = re.search('videoId = \'(.*?)\'', r.content).groups()[0]
        # vid = re.search('iid: (.*?)\r\n', r.content).groups()[0]
        vid = re.search('iid: (\d+)', r.content).groups()[0]
        return vid
    except:
        print traceback.print_exc()
        print 'some wrong with find vid '
        # exit()


def get_danmu(url):
    print url
    url_hash = fnvhash.fnv_64a_str(url)
    vid = get_vid(url)
    print "vid:", vid
    for mat in range(0, 100):
        ri = random.randint(1200, 6000)
        danmu_url = 'http://service.danmu.tudou.com/list?{0}'.format(ri)
        print danmu_url
        dat = {
            'iid': str(vid),
            'ct': '1001',
            'mcount': '1',
            'mat': str(mat),
            'uid': '0',
            'type': "1",
        }
        dr = requests.post(danmu_url, data=dat, timeout=40, headers=
                { 'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36' }
        )
        # with open('danmu.json', 'w') as f:
        #     f.write(dr.content)
        # time.sleep(1)
        try:
            danmu = json.loads(dr.content)
        except:
            return
        count = danmu['count']
        if count == 0:
            print 'count is 0, mat is %s, should exit' % mat
            return
        print "count", count
        r = push_danmu(danmu['result'], url_hash)
        gevent.sleep(4.2)
        if r == -1:
            return
        # print danmu['count']


def filter_emoji(desstr,restr=''):
    '''
    过滤表情
    '''
    try:
        co = re.compile(u'[\U00010000-\U0010ffff]')
    except re.error:
        co = re.compile(u'[\uD800-\uDBFF][\uDC00-\uDFFF]')
    return co.sub(restr, desstr)


def push_danmu(result, url_hash):
    i = 0
    j = 0
    for dan in result:
        d = {}
        d['url_hash'] = url_hash
        d['content'] = filter_emoji(dan['content'])
        d['sentiment'] = get_sentiment_danmu(d['content'])
        d['show_offset'] = dan['playat']
        d['content_hash'] = fnvhash.fnv_32a_str(d['content'].encode('utf8'))
        dc = int(dan['createtime'])
        if dc > 10000000000:
            d['pubtime'] = dan['createtime']/1000
        else:
            d['pubtime'] = dan['createtime']
        id_ = db.item_to_table('danmu', d)
        if id_:
            print '+',
            sys.stdout.flush()
            i += 1
            j = 0
        else:
            print '.',
            sys.stdout.flush()
            j += 1
            if j >= 15:
                print i
                return -1
    print i


if __name__ == '__main__':
    #url = 'http://www.tudou.com/albumplay/lPBkww_XLN8/oMDT3Cn7pKk.html'
    url = 'http://www.tudou.com/albumplay/0GdzKW18i8M/94V7T2RU1Ng.html'
    get_danmu(url)
