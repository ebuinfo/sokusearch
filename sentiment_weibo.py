#!/usr/bin/env python
# encoding: utf8

import re

import pyebunlp


nlp = pyebunlp.EBUnlp('ebunlps.conf')
_p_rmUser = re.compile(r'@.*?(?: |,|:|：|$)', re.DOTALL)


def get_sentiment(text):
    if isinstance(text, unicode):
        text = text.encode('utf8')
    text = _p_rmUser.sub('', text).strip()
    if not text: return 0
    sentiment = nlp.calcDoc(text, 0)
    if sentiment < -5:
        sentiment = -5.0
    elif sentiment > 5:
        sentiment = 5.0
    sentiment = round(sentiment, 2)
    return sentiment


def get_sentiment_danmu(text):
    if isinstance(text, unicode):
        text = text.encode('utf8')
    text = _p_rmUser.sub('', text).strip()
    if not text: return 0
    sentiment = nlp.calcDoc(text, 0)
    if '666' in text:
        sentiment += 2
    if '233' in text:
        sentiment += 2
    if sentiment < -5:
        sentiment = -5.0
    elif sentiment > 5:
        sentiment = 5.0
    sentiment = round(sentiment, 2)
    return sentiment


if __name__ == '__main__':
    text = u'支持凤凰传奇，王迅2333336666666666'
    print get_sentiment_danmu(text)

